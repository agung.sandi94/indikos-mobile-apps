import React, {Component} from 'react';
import {View, Image, TouchableOpacity, Text} from 'react-native';
import FontAwesome from 'react-native-vector-icons/dist/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/dist/MaterialIcons';
import Entypo from 'react-native-vector-icons/dist/Entypo';

export default class Maintabs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bookmark:this.props.bookmark,
      like: this.props.like,
      isVisible: false
    };
  }

  render() {
    return (
      <TouchableOpacity style={{height:175, flexDirection:'row', paddingBottom:10, paddingTop:10, borderColor:'lightgray', borderBottomWidth:1}} onPress={()=>this.props.navigate("IndinewsDetail",{bookmark:this.state.bookmark})}>
        <View style={{width:'37.5%', height:'100%',padding:10}}>
          <Image style={{height:'100%', width:'100%', borderRadius:5}} source={this.props.img}/>
        </View>
        <View style={{width:'62.5%', height:'100%', paddingTop:10, paddingBottom:10}}>
          <View style={{width:'100%', height:'25%', flexDirection:'row', alignItems:'center', justifyContent:'space-between', paddingRight:10}}>
            <Text style={{fontWeight:'bold', color:'black'}}>{this.props.title}</Text>
            <TouchableOpacity style={{alignItems:'center', justifyContent:'center', width:'15%', height:'100%'}} onPress={()=> this.setState({bookmark : !this.state.bookmark})}>
              <Image style={{width:'60%', height:'60%', resizeMode:'contain'}} source={(this.state.bookmark)?require('../assets/img/home/bookmark-active.png'):require('../assets/img/home/bookmark.png')}/>
            </TouchableOpacity>
          </View>
          <View style={{width:'100%', height:'50%', paddingRight:10}}>
            <Text style={{fontSize:12}}>{this.props.headline}</Text>
          </View>
          <View style={{width:'100%', height:'25%', flexDirection:'row', alignItems:'center', justifyContent:'space-between', paddingRight:10 }}>
            <View style={{alignItems:'center', flexDirection:'row'}}>
              <TouchableOpacity onPress={()=>this.setState({like:!this.state.like})}>
                <MaterialIcons name={(this.state.like)?"favorite":"favorite-border"} size={25} color={(this.state.like)?"red":"gray"}/>
              </TouchableOpacity>
              <TouchableOpacity>
                <FontAwesome name={"commenting-o"} size={25} color={"gray"} style={{marginLeft:10}}/>
              </TouchableOpacity>
              <TouchableOpacity>
                <Entypo name={"share"} size={25} color={"gray"} style={{marginLeft:10}}/>
              </TouchableOpacity>
            </View>
              <Text style={{fontSize:12}}>{this.props.time}m ago</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}
