import React, {Component} from 'react';
import {View, Image, TouchableOpacity, FlatList, Dimensions, Text} from 'react-native';

export default class Maintabs extends Component {
  constructor(props) {
    super(props);
    this._changeTabs = this._changeTabs.bind(this);
    this.state = {
      maintabs:'home',
      isVisible: false
    };
  }

  _changeTabs(maintabs){
    this.setState({maintabs})

    // if(maintabs == 'home'){
    //   this.props.navigation.navigate('Home');
    // } else if(maintabs == 'government'){
    //   this.props.navigation.navigate('Government');
    // } else if(maintabs == 'nearme'){
    //   this.props.navigation.navigate('NearMe');
    // } else if(maintabs == 'world'){
    //   this.props.navigation.navigate('Category');
    // } else if(maintabs == 'profile'){
    //   this.props.navigation.navigate('Account');
    // }
  }

  render() {
    return (
      <View style={{width:'100%'}}>
        <View style={{flexDirection:'row', backgroundColor:'white', height:60, width:'100%', position:'absolute', bottom:0, borderColor:'lightgray', borderTopWidth:1}}>
          <TouchableOpacity style={{width:'20%', height:'100%', alignItems:'center', justifyContent:'center'}}
            onPress={()=>{
              this.setState({
                isVisible: false,
                maintabs: "home"
              }, ()=>{this.props.navigate("Home")})
            }}>
            <Image style={{height:'50%',width:'50%', resizeMode:'contain'}} source={(this.state.maintabs=='home')?require('../assets/img/main-tabs/home-active.png'):require('../assets/img/main-tabs/home.png')} />
          </TouchableOpacity>
          <TouchableOpacity style={{width:'20%', height:'100%', alignItems:'center', justifyContent:'center'}}
            onPress={()=>{
              this.setState({
                isVisible: false,
                maintabs: "wishlist"
              }, ()=>{this.props.navigate("Wishlist")})
            }}>
            <Image style={{height:'50%',width:'50%', resizeMode:'contain'}} source={(this.state.maintabs=='wishlist')?require('../assets/img/main-tabs/wishlist-active.png'):require('../assets/img/main-tabs/wishlist.png')} />
          </TouchableOpacity>
          <TouchableOpacity style={{width:'20%', height:'100%', alignItems:'center', justifyContent:'center'}}
            onPress={()=>{
              this.setState({
                isVisible:!this.state.isVisible,
                maintabs: (this.state.isVisible)?"":"menu"
              })
            }}>
            <Image style={{height:'50%',width:'50%', resizeMode:'contain'}} source={(this.state.maintabs=='menu')?require('../assets/img/main-tabs/menu-active.png'):require('../assets/img/main-tabs/menu.png')} />
          </TouchableOpacity>
          <TouchableOpacity style={{width:'20%', height:'100%', alignItems:'center', justifyContent:'center'}}
            onPress={()=>{
              this.setState({
                isVisible: false,
                maintabs: "notification"
              })
            }}>
            <Image style={{height:'50%',width:'50%', resizeMode:'contain'}} source={(this.state.maintabs=='notification')?require('../assets/img/main-tabs/notification-active.png'):require('../assets/img/main-tabs/notification.png')} />
          </TouchableOpacity>
          <TouchableOpacity style={{width:'20%', height:'100%', alignItems:'center', justifyContent:'center'}}
             onPress={()=>{
              this.setState({
                isVisible: false,
                maintabs: "TransactionHistoryList"
              }, ()=>{this.props.navigate("TransactionHistoryList")})
            }}>
            <Image style={{height:'50%',width:'50%', resizeMode:'contain'}} source={(this.state.maintabs=='account')?require('../assets/img/main-tabs/account-active.png'):require('../assets/img/main-tabs/account.png')} />
          </TouchableOpacity>
        </View>
        { (this.state.isVisible) &&
          <View style={{position:'absolute',backgroundColor:'rgba(0,0,0,0.5)', bottom:60, width:'100%', height:Dimensions.get('window').height - 60}}>
            <TouchableOpacity style={{width:'100%', height:'60%'}} onPress={()=>this.setState({isVisible:false, maintabs:""})}></TouchableOpacity>
            <View style={{backgroundColor:'white', width:'100%', height:'40%', borderTopLeftRadius:20, borderTopRightRadius:20}}>
              <View style={{width:'100%', height:'35%'}}>
                <View style={{height:20, width:Dimensions.get('window').width - 40, justifyContent:'center', alignItems:'center', marginLeft:20, marginRight:20}}>
                  <TouchableOpacity style={{backgroundColor:'lightgray', width:'12%', height:'25%'}} onPress={()=>this.setState({isVisible:false})}></TouchableOpacity>
                </View>
                <Text style={{marginLeft:20, marginTop:10}}>Indikos by</Text>
                <Text style={{marginLeft:20, marginTop:5, fontSize:22, fontWeight:'bold', color:'black'}}>Category</Text>
              </View>
              <View style={{width:'100%', height:'65%'}}>
                <View style={{width:'100%', height:'50%', flexDirection:'row'}}>
                  <TouchableOpacity style={{width:'25%', height:'100%', alignItems:'center', justifyContent:'center'}}
                    onPress={()=>{this.setState({isVisible:false}, ()=>{this.props.navigate("IndikosSearch")})}}>
                    <Image style={{width:'50%', height:'50%', resizeMode:'contain'}} source={require('../assets/img/menu/indikos.png')}/>
                    <Text style={{marginTop:5, fontSize:12}}>Indikos</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={{width:'25%', height:'100%', alignItems:'center', justifyContent:'center'}}>
                    <Image style={{width:'50%', height:'50%', resizeMode:'contain'}} source={require('../assets/img/menu/indipulsa.png')}/>
                    <Text style={{marginTop:5, fontSize:12}}>Indipulsa</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={{width:'25%', height:'100%', alignItems:'center', justifyContent:'center'}}>
                    <Image style={{width:'50%', height:'50%', resizeMode:'contain'}} source={require('../assets/img/menu/indineed.png')}/>
                    <Text style={{marginTop:5, fontSize:12}}>Indineed</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={{width:'25%', height:'100%', alignItems:'center', justifyContent:'center'}}>
                    <Image style={{width:'50%', height:'50%', resizeMode:'contain'}} source={require('../assets/img/menu/indilaundry.png')}/>
                    <Text style={{marginTop:5, fontSize:12}}>Indilaundry</Text>
                  </TouchableOpacity>
                </View>
                <View style={{width:'100%', height:'50%', flexDirection:'row'}}>
                  <TouchableOpacity style={{width:'25%', height:'100%', alignItems:'center', justifyContent:'center'}}>
                    <Image style={{width:'50%', height:'50%', resizeMode:'contain'}} source={require('../assets/img/menu/indipick.png')}/>
                    <Text style={{marginTop:5, fontSize:12}}>Indipick</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={{width:'25%', height:'100%', alignItems:'center', justifyContent:'center'}}>
                    <Image style={{width:'50%', height:'50%', resizeMode:'contain'}} source={require('../assets/img/menu/indievent.png')}/>
                    <Text style={{marginTop:5, fontSize:12}}>Indievent</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={{width:'25%', height:'100%', alignItems:'center', justifyContent:'center'}}>
                    <Image style={{width:'50%', height:'50%', resizeMode:'contain'}} source={require('../assets/img/menu/indicareer.png')}/>
                    <Text style={{marginTop:5, fontSize:12}}>Indecareer</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={{width:'25%', height:'100%', alignItems:'center', justifyContent:'center'}}
                    onPress={()=>{this.setState({isVisible:false}, ()=>{this.props.navigate("Indinews")})}}>
                    <Image style={{width:'50%', height:'50%', resizeMode:'contain'}} source={require('../assets/img/menu/indinews.png')}/>
                    <Text style={{marginTop:5, fontSize:12}}>Indinews</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        }
      </View>
    );
  }
}
