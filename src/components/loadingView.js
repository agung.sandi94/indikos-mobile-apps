import React, {Component} from 'react';
import { View, Modal, StyleSheet, ActivityIndicator, Image   } from 'react-native';
import PropTypes from 'prop-types';

class LoadingView extends Component {
  static defaultProps = {
    visible: false
  }

  static propTypes = {
    visible: PropTypes.bool
  };

  render() {
      return  (
        <Modal
          onRequestClose={() => {console.log("close")}}
          supportedOrientations={['landscape', 'portrait']}
          transparent={true}
          visible={this.props.visible}>
          <View style={styles.container}>
            <Image style={{width:'100%', height:'107%'}} source={require("../assets/img/splash-screen.png")} />
            <View style={{width:'100%', height:'100%', position:'absolute', top:0, alignItems:'center', justifyContent:'center'}}>
              <ActivityIndicator size={100} color="#ffffff" style={{marginTop:250}} />
            </View>
          </View>
        </Modal>
      );
  }

}


const styles = StyleSheet.create({
  container: {
    flex:1
  }
})

export default LoadingView;
