import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { addNavigationHelpers, createStackNavigator, createAppContainer } from 'react-navigation';
import Login from './login';
import Register from './register';
import Verification from './verification';
import Home from './home';
import Indinews from './indinews';
import IndinewsDetail from './indinews-detail';
import IndikosSearch from './indikos-search';
import KosanDetail from './kosan-detail';
import Maps from './maps';
import IndineedsSearch from './indineeds-search';
import NeedsDetail from './needs-detail';
import TransactionHistoryList from './transaction-history-list';
import KosanList from './kosan-list';
import NeedList from './need-list';
import Wishlist from './wishlist';

import Example from './example';

const AppNavigator = createStackNavigator({
  // Example: { screen: Example },
  Login: { screen: Login },
  Register: { screen: Register},
  Verification: { screen: Verification},
  Home: { screen: Home },
  Indinews: { screen: Indinews },
  IndinewsDetail: { screen: IndinewsDetail },
  IndikosSearch: { screen: IndikosSearch },
  KosanDetail: { screen: KosanDetail },
  Maps: { screen: Maps },
  IndineedsSearch: { screen: IndineedsSearch },
  NeedsDetail: { screen: NeedsDetail },
  TransactionHistoryList: { screen: TransactionHistoryList },
  KosanList: { screen: KosanList },
  NeedList: {screen: NeedList},
  Wishlist: {screen: Wishlist}

});

const AppContainer = createAppContainer(AppNavigator);

const mapStateToProps = state => ({
  // nav: state.navReducer,
});

export default connect(mapStateToProps)(AppContainer);
