import React, {Component} from 'react';
import { Platform, StyleSheet, Text, View, Dimensions, ScrollView, Image, TextInput, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { finishCheck } from '../login/actions'
import Maintabs from '../../components/maintabs';
import FontAwesome from 'react-native-vector-icons/dist/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/dist/FontAwesome5';
import Ionicons from 'react-native-vector-icons/dist/Ionicons';
import Octicons from 'react-native-vector-icons/dist/Octicons';
import Picker from 'react-native-picker';
import Slider from "react-native-slider";
import { TabNavigator } from 'react-navigation';

class Wishlist extends Component {
  static navigationOptions = {
    title: "Wishlist" ,
    headerBackTitle: 'Back'
  };

  constructor(props) {
    super(props);
    this._showTypePicker = this._showTypePicker.bind(this);
    this._showDurationPicker = this._showDurationPicker.bind(this);
    this.state = {
      text: "",
      activeTab:1,
      type: "Men",
      duration: "Per Year",
      value:250000
    }
  }

  _showTypePicker() {
      Picker.init({
          pickerData: ['Men','Woman'],
          selectedValue: ['Men'],
          pickerTitleText:"Men",
          onPickerConfirm: pickedValue => {
              this.setState({
                  type:pickedValue
              })
          },
          onPickerCancel: pickedValue => {
              console.log( pickedValue);
          },
          onPickerSelect: pickedValue => {
              console.log( pickedValue);

          }
      });
      Picker.show();
  }

  _showDurationPicker() {
      Picker.init({
          pickerData: ['Per Year','Per Month','Per Day'],
          selectedValue: ['Per Year'],
          pickerTitleText:"Per Year",
          onPickerConfirm: pickedValue => {
              this.setState({
                  duration:pickedValue[0]
              })
          },
          onPickerCancel: pickedValue => {
              console.log( pickedValue);
          },
          onPickerSelect: pickedValue => {
              console.log( pickedValue);

          }
      });
      Picker.show();
  }

  

  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={{height:'100%', width:'100%', marginBottom:60, backgroundColor:'white'}}>
          
          

          <View style={{padding:20}}>
            <ScrollView horizontal={true} style={{borderTopColor:'lightgray', borderBottomColor:'lightgray', borderTopWidth:1, borderBottomWidth:1}}>
              <TouchableOpacity onPress={()=> this.setState({activeTab:1})} style={{borderTopColor:'lightgray', borderBottomColor:(this.state.activeTab==1)?'#65c5f2':'lightgray', borderBottomWidth:(this.state.activeTab==1)?2:0, alignItems:'center', justifyContent:'center', padding:10, paddingLeft:13}} onPress={()=>this.props.navigation.navigate("KosanList")}>
                <Text style={{color:(this.state.activeTab==1)?'#65c5f2':'black', fontWeight:(this.state.activeTab==1)?'bold':'500'}}>Indikos</Text>  
              </TouchableOpacity>
              <TouchableOpacity onPress={()=> this.setState({activeTab:2})} style={{borderTopColor:'lightgray', borderBottomColor:(this.state.activeTab==2)?'#65c5f2':'lightgray', borderBottomWidth:(this.state.activeTab==2)?2:0, alignItems:'center', justifyContent:'center', padding:10, paddingLeft:13}} onPress={()=>this.props.navigation.navigate("NeedList")}>
                <Text style={{color:(this.state.activeTab==2)?'#65c5f2':'black', fontWeight:(this.state.activeTab==2)?'bold':'500'}}>Indineed</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=> this.setState({activeTab:3})} style={{borderTopColor:'lightgray', borderBottomColor:(this.state.activeTab==3)?'#65c5f2':'lightgray', borderBottomWidth:(this.state.activeTab==3)?2:0, alignItems:'center', justifyContent:'center', padding:10, paddingLeft:13}}>
                <Text style={{color:(this.state.activeTab==3)?'#65c5f2':'black', fontWeight:(this.state.activeTab==3)?'bold':'500'}}>Indilaundry</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=> this.setState({activeTab:4})} style={{borderTopColor:'lightgray', borderBottomColor:(this.state.activeTab==4)?'#65c5f2':'lightgray', borderBottomWidth:(this.state.activeTab==4)?2:0, alignItems:'center', justifyContent:'center', padding:10, paddingLeft:13}}>
                <Text style={{color:(this.state.activeTab==4)?'#65c5f2':'black', fontWeight:(this.state.activeTab==4)?'bold':'500'}}>Indipulsa</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=> this.setState({activeTab:5})} style={{borderTopColor:'lightgray', borderBottomColor:(this.state.activeTab==5)?'#65c5f2':'lightgray', borderBottomWidth:(this.state.activeTab==5)?2:0, alignItems:'center', justifyContent:'center', padding:10, paddingLeft:13}}>
                <Text style={{color:(this.state.activeTab==5)?'#65c5f2':'black', fontWeight:(this.state.activeTab==5)?'bold':'500'}}>Indipick</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=> this.setState({activeTab:6})} style={{borderTopColor:'lightgray', borderBottomColor:(this.state.activeTab==6)?'#65c5f2':'lightgray', borderBottomWidth:(this.state.activeTab==6)?2:0, alignItems:'center', justifyContent:'center', padding:10, paddingLeft:13}}>
                <Text style={{color:(this.state.activeTab==6)?'#65c5f2':'black', fontWeight:(this.state.activeTab==6)?'bold':'500'}}>Indievent</Text>
              </TouchableOpacity>
            </ScrollView>
            </View>
          <View style={{padding:20}}>
            <TouchableOpacity onPress={()=>this.props.navigation.navigate('KosanDetail')} style={{marginTop:30, marginBottom:30, height:200, borderRadius:10, borderColor:'lightgray', borderWidth:1, flexDirection:'row', justifyContent:'space-between'}}>
              <View style={{height:'100%', width:'40%'}}>
                <View style={{position:'absolute', bottom:0, height:'115%', width:'100%', zIndex:1}}>
                  <Image style={{height:'100%', width:'100%', borderRadius:10, borderBottomRightRadius:0}} source={require("../../assets/img/indikos-search/kosan-1.png")}/>
                  <View style={{position:'absolute', bottom: 0, left: '10%', width:'80%', height:'18%', backgroundColor:'#65c5f2', alignItems:'center', justifyContent:'center', borderTopLeftRadius: 5, borderTopRightRadius: 5}}>
                    <Text style={{color:'white', fontWeight:'bold'}}>Khusus Putra</Text>
                  </View>
                </View>
              </View>
              <View style={{height:'100%', width:'60%', padding:10}}>
                <View style={{height:'70%', borderBottomColor:'lightgray', borderBottomWidth:1}}>
                  <Text style={{fontWeight:'bold', color:'black', fontSize:16, marginTop:5}}>Kosan Blockchain</Text>
                  <View style={{flexDirection:'row', marginTop:5}}>
                    <Octicons name={"location"} size={14} color={"#65c5f2"}/>
                    <Text style={{fontSize:12, marginLeft:5, marginRight:5}}>Jl. Lorem Ipsum e un testo tempo segnaposto utilizzato ...</Text>
                  </View>
                  <ScrollView horizontal={true} style={{marginTop:10, marginBottom:10}}>
                    <View style={{alignItems:'center', justifyContent:'center'}}>
                      <Image style={{height:20, width:20, resizeMode:'contain'}} source={require("../../assets/img/kosan-detail/facilities/fan.png")}/>
                    </View>
                    <View style={{alignItems:'center', justifyContent:'center', marginLeft:10}}>
                      <Image style={{height:20, width:20, resizeMode:'contain'}} source={require("../../assets/img/kosan-detail/facilities/security.png")}/>
                    </View>
                    <View style={{alignItems:'center', justifyContent:'center', marginLeft:10}}>
                      <Image style={{height:20, width:20, resizeMode:'contain'}} source={require("../../assets/img/kosan-detail/facilities/bed.png")}/>
                    </View>
                    <View style={{alignItems:'center', justifyContent:'center', marginLeft:10}}>
                      <Image style={{height:20, width:20, resizeMode:'contain'}} source={require("../../assets/img/kosan-detail/facilities/wardrobe.png")}/>
                    </View>
                  </ScrollView>
                </View>
                <View style={{height:'30%', flexDirection:'row', alignItems:'center', justifyContent:'space-between', paddingTop:10}}>
                  <Text style={{color:'#65c5f2', fontWeight:'800'}}>Rp 500.000</Text>
                  <TouchableOpacity style={{padding:10, borderRadius:20, borderColor:'#65c5f2', borderWidth:1, alignItems:'center', justifyContent:'center'}}>
                    <Text style={{color:'#65c5f2', fontWeight:'500'}}>Book Now</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>this.props.navigation.navigate('KosanDetail')} style={{marginTop:30, marginBottom:30, height:200, borderRadius:10, borderColor:'lightgray', borderWidth:1, flexDirection:'row', justifyContent:'space-between'}}>
              <View style={{height:'100%', width:'40%'}}>
                <View style={{position:'absolute', bottom:0, height:'115%', width:'100%', zIndex:1}}>
                  <Image style={{height:'100%', width:'100%', borderRadius:10, borderBottomRightRadius:0}} source={require("../../assets/img/indikos-search/kosan-2.png")}/>
                  <View style={{position:'absolute', bottom: 0, left: '10%', width:'80%', height:'18%', backgroundColor:'#65c5f2', alignItems:'center', justifyContent:'center', borderTopLeftRadius: 5, borderTopRightRadius: 5}}>
                    <Text style={{color:'white', fontWeight:'bold'}}>Khusus Putri</Text>
                  </View>
                </View>
              </View>
              <View style={{height:'100%', width:'60%', padding:10}}>
                <View style={{height:'70%', borderBottomColor:'lightgray', borderBottomWidth:1}}>
                  <Text style={{fontWeight:'bold', color:'black', fontSize:16, marginTop:5}}>Kosan Blockchain</Text>
                  <View style={{flexDirection:'row', marginTop:5}}>
                    <Octicons name={"location"} size={14} color={"#65c5f2"}/>
                    <Text style={{fontSize:12, marginLeft:5, marginRight:5}}>Jl. Lorem Ipsum e un testo tempo segnaposto utilizzato ...</Text>
                  </View>
                  <ScrollView horizontal={true} style={{marginTop:10, marginBottom:10}}>
                    <View style={{alignItems:'center', justifyContent:'center'}}>
                      <Image style={{height:20, width:20, resizeMode:'contain'}} source={require("../../assets/img/kosan-detail/facilities/fan.png")}/>
                    </View>
                    <View style={{alignItems:'center', justifyContent:'center', marginLeft:10}}>
                      <Image style={{height:20, width:20, resizeMode:'contain'}} source={require("../../assets/img/kosan-detail/facilities/security.png")}/>
                    </View>
                    <View style={{alignItems:'center', justifyContent:'center', marginLeft:10}}>
                      <Image style={{height:20, width:20, resizeMode:'contain'}} source={require("../../assets/img/kosan-detail/facilities/bed.png")}/>
                    </View>
                    <View style={{alignItems:'center', justifyContent:'center', marginLeft:10}}>
                      <Image style={{height:20, width:20, resizeMode:'contain'}} source={require("../../assets/img/kosan-detail/facilities/wardrobe.png")}/>
                    </View>
                  </ScrollView>
                </View>
                <View style={{height:'30%', flexDirection:'row', alignItems:'center', justifyContent:'space-between', paddingTop:10}}>
                  <Text style={{color:'#65c5f2', fontWeight:'800'}}>Rp 500.000</Text>
                  <TouchableOpacity style={{padding:10, borderRadius:20, borderColor:'#65c5f2', borderWidth:1, alignItems:'center', justifyContent:'center'}}>
                    <Text style={{color:'#65c5f2', fontWeight:'500'}}>Book Now</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </ScrollView>
        <Maintabs
          navigate={this.props.navigation.navigate}
        />
      </View>
    );
  }
}

const styles= StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#eaf2ff',
  },
  oval: {
      height: Dimensions.get('window').width * 2,
      width: Dimensions.get('window').width * 3,
      marginLeft: -(Dimensions.get('window').width),
      borderBottomLeftRadius: Dimensions.get('window').width*1.5,
      borderBottomRightRadius: Dimensions.get('window').width*1.5,
      backgroundColor: 'rgba(26, 175, 244, 0.75)',
      position: 'absolute',
      top: -(Dimensions.get('window').width*0.4),
  },
});

const mapStateToProps = (state) => {
    return {
        data: state.loginReducer.data,
        isFetching: state.loginReducer.isFetching,
        error: state.loginReducer.error,
        message: state.loginReducer.message,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        finishCheck: () => dispatch(finishCheck()),
    };
};

export default connect(mapStateToProps,mapDispatchToProps)(Wishlist);
