import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image, TouchableOpacity, TextInput, Alert, ScrollView, FlatList, Dimensions} from 'react-native';
import { connect } from 'react-redux';
import Maintabs from '../../components/maintabs';
import Ionicons from 'react-native-vector-icons/dist/Ionicons';
import Entypo from 'react-native-vector-icons/dist/Entypo';
import FontAwesome from 'react-native-vector-icons/dist/FontAwesome';
import Octicons from 'react-native-vector-icons/dist/Octicons';
import { Rating } from 'react-native-elements';
import SplashScreen from 'react-native-smart-splash-screen';

class KosanDetail extends Component {
  static navigationOptions = ({navigation}) => ({
    header: null ,
    headerBackTitle: 'Back',
  });

  constructor(props) {
    super(props);
    this.state = {
      activeTab:1
    }
  }

  componentDidMount () {
       //SplashScreen.close(SplashScreen.animationType.scale, 850, 500)
       SplashScreen.close({
          animationType: SplashScreen.animationType.scale,
          duration: 850,
          delay: 500,
       })
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={{flex:1, width:'100%', marginBottom:60, backgroundColor:'white'}}>
          <View>
            <ScrollView horizontal={true} pagingEnabled={true}>
              <Image style={{height:250, width:Dimensions.get('window').width, resizeMode:'stretch'}} source={require("../../assets/img/kosan-detail/kosan.png")}/>
              <Image style={{height:250, width:Dimensions.get('window').width, resizeMode:'stretch'}} source={require("../../assets/img/kosan-detail/kosan.png")}/>
              <Image style={{height:250, width:Dimensions.get('window').width, resizeMode:'stretch'}} source={require("../../assets/img/kosan-detail/kosan.png")}/>
              <Image style={{height:250, width:Dimensions.get('window').width, resizeMode:'stretch'}} source={require("../../assets/img/kosan-detail/kosan.png")}/>
            </ScrollView>
            <TouchableOpacity style={{position:'absolute', top:20, left: 20}} onPress={()=>this.props.navigation.goBack()}>
              <Ionicons name={"ios-arrow-back"} size={35} color={"white"} />
            </TouchableOpacity>
            <TouchableOpacity style={{position:'absolute', top:20, right: 20}}>
              <Entypo name={"share"} size={30} color={"white"}/>
            </TouchableOpacity>
            <View style={{position:'absolute', bottom:40, width:'100%', justifyContent:'center', alignItems:'center', flexDirection:'row'}}>
              <FontAwesome name={"minus"} size={24} color={"white"}/>
              <FontAwesome name={"circle"} size={8} color={"lightgray"} style={{marginLeft:5}}/>
              <FontAwesome name={"circle"} size={8} color={"lightgray"} style={{marginLeft:5}}/>
              <FontAwesome name={"circle"} size={8} color={"lightgray"} style={{marginLeft:5}}/>
            </View>
          </View>
          <View style={{ width:'100%', alignItems:'center', justifyContent:'center', height:190}}>
            <View style={{position:"absolute", top:-40, width:'90%', backgroundColor:'white', borderRadius:10, flexDirection:'row', padding:10, alignItems:'center', justifyContent:'space-between', borderColor:'lightgray', borderWidth:1}}>
              <View style={{ justifyContent:'center', padding:10}}>
                <Text style={{fontSize:18, fontWeight:'bold', color:'black'}}>LA Sheraton Kosan</Text>
                <View style={{flexDirection:'row', alignItems:'center', marginTop:5}}>
                  <Rating
                    type="custom"
                    ratingColor="orange"
                    fractions={1}
                    startingValue={4.5}
                    readonly
                    imageSize={12}
                  />
                  <Text style={{fontSize:12, color:'lightgray', marginLeft:5}}>4.5/5 (12 Tinjauan)</Text>
                </View>
                <View style={{flexDirection:'row', alignItems:'center', marginTop:5}}>
                  <Octicons name={"location"} size={12} color={"lightgray"}/>
                  <Text style={{fontSize:12, color:'lightgray', marginLeft:5}}>Jl. Los Angeles, USA (1,2 km from centre)</Text>
                </View>
              </View>
              <View style={{alignItems:'center', justifyContent:'center'}}>
                <TouchableOpacity onPress={()=>this.props.navigation.navigate("Maps")}>
                  <Image style={{height:40, width:40, resizeMode:'contain'}} source={require("../../assets/img/kosan-detail/next-to-maps.png")} />
                </TouchableOpacity>
              </View>
            </View>
            <View style={{position:'absolute', bottom:40, width:'90%', backgroundColor:'white', borderRadius:10, flexDirection:'row', padding:15, alignItems:'center', justifyContent:'space-between', borderColor:'lightgray', borderWidth:1}}>
              <Text style={{fontSize:16, fontWeight:'400', marginLeft:10}}>IDR 1.200.000/ Year</Text>
              <Image style={{height:25,width:25, marginRight:10}} source={require("../../assets/img/kosan-detail/wallet.png")} />
            </View>
          </View>
          <View style={{padding:10}}>
            <ScrollView horizontal={true} style={{borderTopColor:'lightgray', borderBottomColor:'lightgray', borderTopWidth:1, borderBottomWidth:1}}>
              <TouchableOpacity onPress={()=> this.setState({activeTab:1})} style={{borderTopColor:'lightgray', borderBottomColor:(this.state.activeTab==1)?'#65c5f2':'lightgray', borderBottomWidth:(this.state.activeTab==1)?2:0, alignItems:'center', justifyContent:'center', padding:10, paddingLeft:0}}>
                <Text style={{color:(this.state.activeTab==1)?'#65c5f2':'black', fontWeight:(this.state.activeTab==1)?'bold':'400'}}>Per Year</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=> this.setState({activeTab:2})} style={{borderTopColor:'lightgray', borderBottomColor:(this.state.activeTab==2)?'#65c5f2':'lightgray', borderBottomWidth:(this.state.activeTab==2)?2:0, alignItems:'center', justifyContent:'center', padding:10, paddingLeft:0}}>
                <Text style={{color:(this.state.activeTab==2)?'#65c5f2':'black', fontWeight:(this.state.activeTab==2)?'bold':'400'}}>Per Month</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=> this.setState({activeTab:3})} style={{borderTopColor:'lightgray', borderBottomColor:(this.state.activeTab==3)?'#65c5f2':'lightgray', borderBottomWidth:(this.state.activeTab==3)?2:0, alignItems:'center', justifyContent:'center', padding:10, paddingLeft:0}}>
                <Text style={{color:(this.state.activeTab==3)?'#65c5f2':'black', fontWeight:(this.state.activeTab==3)?'bold':'400'}}>Per Day</Text>
              </TouchableOpacity>
            </ScrollView>
          </View>
          <View style={{padding:10, flexDirection:'row', justifyContent:'space-between', alignItems:'center', paddingTop:30, paddingBottom:30}}>
            <View style={{flexDirection:'row', alignItems:'center'}}>
              <Image style={{width:50, height:50}} source={require("../../assets/img/kosan-detail/type-kosan.png")} />
              <Text style={{marginLeft:5, fontWeight:'bold', fontSize:16, color:'black'}}>Khusus Putra</Text>
            </View>
            <View style={{flexDirection:'row', alignItems:'center', justifyContent:'flex-end'}}>
              <Image style={{width:25, height:25}} source={require("../../assets/img/kosan-detail/premium.png")} />
              <Text style={{marginLeft:5, fontWeight:'bold', fontSize:16, color:'#65c5f2'}}>Premium</Text>
            </View>
          </View>
          <View style={{padding:10}}>
            <View style={{borderColor:'lightgray', borderTopWidth:1, borderBottomWidth:1}}>
              <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center', padding:10, paddingBottom:5}}>
                <Text>Kosan available</Text>
                <Text style={{marginLeft:5, fontWeight:'bold', color:'black'}}>10 Rooms</Text>
              </View>
              <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center', padding:10, paddingTop:5, paddingBottom:5}}>
                <Text>Unit area</Text>
                <View style={{flexDirection:'row', alignItems: 'flex-start'}}>
                  <Text style={{marginLeft:5, fontWeight:'bold', color:'black'}}>24 M</Text>
                  <Text style={{fontWeight:'bold', color:'black', lineHeight: 10, fontSize:10}}>2</Text>
                </View>
              </View>
              <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center', padding:10, paddingTop:5}}>
                <Text>Many views</Text>
                <Text style={{marginLeft:5, fontWeight:'bold', color:'black'}}>90 View</Text>
              </View>
            </View>
          </View>
          <View style={{padding:15}}>
            <Text style={{marginTop:10, fontWeight:'bold', fontSize:16, color:'black'}}>Facilities</Text>
            <ScrollView horizontal={true} style={{marginTop:10}}>
              <View style={{padding:10, alignItems:'center', justifyContent:'center'}}>
                <View style={{alignItems:'center', justifyContent:'center', padding:10, backgroundColor:'#f4f7fc', borderRadius:5 }}>
                  <Image style={{height:35, width:35, resizeMode:'contain'}} source={require("../../assets/img/kosan-detail/facilities/fan.png")}/>
                </View>
                <Text style={{marginTop:5, marginBottom:5}}>Fan</Text>
              </View>
              <View style={{padding:10, alignItems:'center', justifyContent:'center'}}>
                <View style={{alignItems:'center', justifyContent:'center', padding:10, backgroundColor:'#f4f7fc', borderRadius:5 }}>
                  <Image style={{height:35, width:35, resizeMode:'contain'}} source={require("../../assets/img/kosan-detail/facilities/security.png")}/>
                </View>
                <Text style={{marginTop:5, marginBottom:5}}>Security</Text>
              </View>
              <View style={{padding:10, alignItems:'center', justifyContent:'center'}}>
                <View style={{alignItems:'center', justifyContent:'center', padding:10, backgroundColor:'#f4f7fc', borderRadius:5 }}>
                  <Image style={{height:35, width:35, resizeMode:'contain'}} source={require("../../assets/img/kosan-detail/facilities/bed.png")}/>
                </View>
                <Text style={{marginTop:5, marginBottom:5}}>Bed</Text>
              </View>
              <View style={{padding:10, alignItems:'center', justifyContent:'center'}}>
                <View style={{alignItems:'center', justifyContent:'center', padding:10, backgroundColor:'#f4f7fc', borderRadius:5 }}>
                  <Image style={{height:35, width:35, resizeMode:'contain'}} source={require("../../assets/img/kosan-detail/facilities/wardrobe.png")}/>
                </View>
                <Text style={{marginTop:5, marginBottom:5}}>Wardrobe</Text>
              </View>
              <View style={{padding:10, alignItems:'center', justifyContent:'center'}}>
                <View style={{alignItems:'center', justifyContent:'center', padding:10, backgroundColor:'#f4f7fc', borderRadius:5 }}>
                  <Image style={{height:35, width:35, resizeMode:'contain'}} source={require("../../assets/img/kosan-detail/facilities/washer.png")}/>
                </View>
                <Text style={{marginTop:5, marginBottom:5}}>Washer</Text>
              </View>
              <View style={{padding:10, alignItems:'center', justifyContent:'center'}}>
                <View style={{alignItems:'center', justifyContent:'center', padding:10, backgroundColor:'#f4f7fc', borderRadius:5 }}>
                  <Image style={{height:35, width:35, resizeMode:'contain'}} source={require("../../assets/img/kosan-detail/facilities/bath.png")}/>
                </View>
                <Text style={{marginTop:5, marginBottom:5}}>Bath</Text>
              </View>
            </ScrollView>
          </View>
          <View style={{padding:15}}>
            <Text style={{marginTop:10, fontWeight:'bold', fontSize:16, color:'black'}}>Facilities</Text>
            <Text style={{marginTop:10}}>E universalmente riconosciuto che un lettore che osserva il layout di una pagina viene distratto dal contenuto testuale se questo e leggibile. Lo scopo dell’utilizzo del Lorem Ipsum e che offre una nomale distribuzione delle lettere (al contrario di quanto avviene se si utilizzano brevi frasi ripetute, ad esempio “testo qui”), apparendo come un nomale blocco di testo leggibile. {'\n \n'}Molti software di impaginazione e di web design utilizzano Lorem Ipsum come testo modello. Molte versioni del testo sono state prodotte negli anni, a volte casualmente, a volte di proposito (ad esempio inserendo passiggi ironici).</Text>
          </View>
          <View style={{padding:15, marginTop:10}}>
            <TouchableOpacity style={{alignItems:'center', justifyContent:'center', padding:10, borderColor:'#65c5f2', borderRadius:5, borderWidth:2}}>
              <Text style={{color:'#65c5f2', fontWeight:'bold'}}>Survey Request</Text>
            </TouchableOpacity>
          </View>
          <View style={{marginTop:25, flexDirection:'row'}}>
            <TouchableOpacity style={{alignItems:'center', justifyContent:'center', width:'80%', flexDirection:'row', backgroundColor:'#65c5f2', padding:15}}>
              <Text style={{color:'white', fontWeight:'bold', fontSize:18}}>Book Now</Text>
              <FontAwesome name={"long-arrow-right"} size={30} color={"white"} style={{marginLeft:10, marginTop:5}}/>
            </TouchableOpacity>
            <TouchableOpacity style={{alignItems:'center', justifyContent:'center', width:'20%'}}>
              <Entypo name={"phone"} size={30} color={"#65c5f2"}/>
            </TouchableOpacity>
          </View>
        </ScrollView>
        <Maintabs
          navigate={this.props.navigation.navigate}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#eaf2ff',
  },
  content:{
    flex:1,
    width:'100%',
  },
  imgUser:{
    width: 130,
    height: 130,
    borderRadius: 65
  },
});

const mapStateToProps = (state) => {
    return {
        data: state.loginReducer.data,
        isFetching: state.loginReducer.isFetching,
        error: state.loginReducer.error,
        message: state.loginReducer.message,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {

    };
};

export default connect(mapStateToProps,mapDispatchToProps)(KosanDetail);
