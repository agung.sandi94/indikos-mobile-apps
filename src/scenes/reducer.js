import { combineReducers } from 'redux'
import { reducer as loginReducer } from './login/reducer'
import { reducer as homeReducer } from './home/reducer'

export default combineReducers({
  loginReducer,
  homeReducer
})
