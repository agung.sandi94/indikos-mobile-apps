import {AUTH} from '../../services'

export const LOGIN_STATE = {
  FETCHING_DATA: 'FETCHING_LOGIN_DATA',
  FETCHING_DATA_SUCCESS: 'FETCHING_LOGIN_DATA_SUCCESS',
  FETCHING_DATA_FAILURE: 'FETCHING_LOGIN_DATA_FAILURE',
  CHECKING_DATA_FINISH: 'CHECKING_LOGIN_DATA_FINISH'
}

function getData() {
  return {
    type: LOGIN_STATE.FETCHING_DATA
  }
}

function getDataSuccess(data) {
  return {
    type: LOGIN_STATE.FETCHING_DATA_SUCCESS,
    data: data,
  }
}

function getDataFailure(message) {
  return {
    type: LOGIN_STATE.FETCHING_DATA_FAILURE,
    message: message
  }
}

export function finishCheck(){
  return {
    type: LOGIN_STATE.CHECKING_DATA_FINISH
  }
}

export function login(username,password) {
  return (dispatch) => {
    dispatch(getData())
    AUTH.login(username,password)
      .then((data) => {
        result = data.data;

        if (data.status == 200){
            dispatch(getDataSuccess(result.data))
        } else {
            dispatch(getDataFailure(result.message))
        }

      })
      .catch((err) => {
        dispatch(getDataFailure(err.data.message))
      })
  }
}
