import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TouchableOpacity, TextInput, Image, ScrollView, Dimensions, Modal} from 'react-native';
import { connect } from 'react-redux';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import { Marker } from 'react-native-maps';
import getDirections from 'react-native-google-maps-directions';
import { styles } from "../../components/stylesheet";
import { Rating } from 'react-native-elements';

class Maps extends Component {
  static navigationOptions = {
    title: "Maps",
    headerBackTitle: 'Back',
  };

  constructor(props) {
    super(props);
    this.state = {
      region: {
        latitude: -6.9090874,
        longitude: 107.598978,
        latitudeDelta: 0.0422,
        longitudeDelta: 0.0221,
      },
      initialRegion: {
        latitude: -6.9090874,
        longitude: 107.598978,
        latitudeDelta: 0.0422,
        longitudeDelta: 0.0221,
      },
      selected: null ,
      marker: {
          title: "My Location",
          coordinates: {
            latitude: -6.8964,
            longitude: 107.5989
          }
      },
      loading: false,
    }
  }

  componentDidMount(){
    // var initialRegion = this.state.initialRegion;
    navigator.geolocation.getCurrentPosition(
       (position) => {
         var region = {
           latitude: position.coords.latitude,
           longitude: position.coords.longitude,
           latitudeDelta: 0.0422,
           longitudeDelta: 0.0221,
         }

         var marker = this.state.marker;

         marker.coordinates.latitude = position.coords.latitude;
         marker.coordinates.longitude = position.coords.longitude;
         this.setState({
           region:region,
           initialRegion:region,
           marker: marker
         });
         // fetch('https://maps.googleapis.com/maps/api/geocode/json?address=' + position.coords.latitude + ',' + position.coords.longitude + '&key=AIzaSyASdx8sGVk9_lUodG2QMggEkERWEZ0dBE4')
         //    .then((response) => response.json())
         //    .then((responseJson) => {
         //        // console.log('ADDRESS GEOCODE is BACK!! => ' + JSON.stringify(responseJson));
         //        // console.log(responseJson.results[responseJson.results.length-4].address_components[0].long_name);
         //        this.setState({
         //          city:responseJson.results[responseJson.results.length-4].address_components[0].long_name,
         //          latitude: position.coords.latitude,
         //          longitude: position.coords.longitude,
         //          error: null,
         //        });
         // })
       },
       (error) => {
         this.setState({ error: error.message })
       },
     );
  }

  _onRegionChange(region) {
    this.setState({ region });
  }

  _getDirections(){
    const data = {
       source: {
        latitude: this.state.initialRegion.latitude,
        longitude: this.state.initialRegion.longitude
      },
      destination: {
        latitude: this.state.selected.coordinates.latitude,
        longitude: this.state.selected.coordinates.longitude
      },
      params: [
        {
          key: "travelmode",
          value: "driving"        // may be "walking", "bicycling" or "transit" as well
        },
        {
          key: "dir_action",
          value: "navigate"       // this instantly initializes navigation using the given travel mode
        }
      ]
    }

    getDirections(data)
  }

  render() {
    return (
      <View style={stylesMaps.container}>
        <MapView
          onPress={()=>{
            this.setState({selected:null});
          }}
          style={{height:'100%', width:'100%'}}
          region={this.state.region}>
            <Marker
              coordinate={this.state.marker.coordinates}
              title={this.state.marker.title}
              image={require("../../assets/img/pinloc.png")}
              onPress={() => {
                var region = {
                  latitude: this.state.marker.coordinates.latitude,
                  longitude: this.state.marker.coordinates.longitude,
                  latitudeDelta: 0.0422,
                  longitudeDelta: 0.0221,
                }
                this.setState({
                  region: region,
                  selected: this.state.marker
                });

              }}>
            </Marker>
        </MapView>
        <View style={{position:'absolute', bottom:0, backgroundColor:'white', width:'100%', height:'25%', borderTopLeftRadius:10, borderTopRightRadius:10, padding:10}}>
          <Text style={{fontSize:22, fontWeight:'bold', marginTop:10}}>Kosan Al contrario di quanto si pensi parte hanno</Text>
          <TouchableOpacity style={{marginTop:10}}>
            <Text style={[styles.hyperlinkText,{textDecorationLine:'underline'}]}>Jl. Esistono innumerevoli variazioni dei passaggi del Lorem Ipsum</Text>
          </TouchableOpacity>
          <View style={{flexDirection:'row', alignItems:'center', marginTop:10}}>
            <Rating
              type="custom"
              ratingColor="orange"
              fractions={1}
              startingValue={4.5}
              readonly
              imageSize={12}
            />
            <Text style={{marginLeft:5}}>4.5/5 (12 Tinjauan)</Text>
          </View>
        </View>
      </View>
    );
  }
}

const stylesMaps = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#eaf2ff',
  },
});

const mapStateToProps = (state) => {
    return {};
};

const mapDispatchToProps = (dispatch) => {
    return {};
};

export default connect(mapStateToProps,mapDispatchToProps)(Maps);
