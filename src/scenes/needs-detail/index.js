import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image, TouchableOpacity, TextInput, Alert, ScrollView, FlatList, Dimensions} from 'react-native';
import { connect } from 'react-redux';
import Maintabs from '../../components/maintabs';
import Ionicons from 'react-native-vector-icons/dist/Ionicons';
import Entypo from 'react-native-vector-icons/dist/Entypo';
import FontAwesome from 'react-native-vector-icons/dist/FontAwesome';
import Octicons from 'react-native-vector-icons/dist/Octicons';
import { Rating } from 'react-native-elements';
import SplashScreen from 'react-native-smart-splash-screen';


class NeedsDetail extends Component {
  static navigationOptions = ({navigation}) => ({
    header: null ,
    headerBackTitle: 'Back',
  });

  constructor(props) {
    super(props);
    this.state = {
      activeTab:1
    }
  }

  componentDidMount () {
       //SplashScreen.close(SplashScreen.animationType.scale, 850, 500)
       SplashScreen.close({
          animationType: SplashScreen.animationType.scale,
          duration: 850,
          delay: 500,
       })
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={{flex:1, width:'100%', marginBottom:60, backgroundColor:'white'}}>
          <View>
            <ScrollView horizontal={true} pagingEnabled={true}>
              <Image style={{height:300, width:Dimensions.get('window').width, resizeMode:'stretch'}} source={require("../../assets/img/kosan-detail/kosan.png")}/>
              <Image style={{height:300, width:Dimensions.get('window').width, resizeMode:'stretch'}} source={require("../../assets/img/kosan-detail/kosan.png")}/>
              <Image style={{height:300, width:Dimensions.get('window').width, resizeMode:'stretch'}} source={require("../../assets/img/kosan-detail/kosan.png")}/>
              <Image style={{height:300, width:Dimensions.get('window').width, resizeMode:'stretch'}} source={require("../../assets/img/kosan-detail/kosan.png")}/>
            </ScrollView>
            <TouchableOpacity style={{position:'absolute', top:20, left: 20}} onPress={()=>this.props.navigation.goBack()}>
              <Ionicons name={"ios-arrow-back"} size={35} color={"white"} />
            </TouchableOpacity>
            <TouchableOpacity style={{position:'absolute', top:20, right: 20}}>
              <Entypo name={"share"} size={30} color={"white"}/>
            </TouchableOpacity>
            <View style={{position:'absolute', bottom:40, width:'100%', justifyContent:'center', alignItems:'center', flexDirection:'row'}}>
              <FontAwesome name={"minus"} size={24} color={"white"}/>
              <FontAwesome name={"circle"} size={8} color={"lightgray"} style={{marginLeft:5}}/>
              <FontAwesome name={"circle"} size={8} color={"lightgray"} style={{marginLeft:5}}/>
              <FontAwesome name={"circle"} size={8} color={"lightgray"} style={{marginLeft:5}}/>
            </View>
          </View>
          <View style={{ width:'100%', alignItems:'flex-end', justifyContent:'center', height:20}}>
            <View style={{position:"absolute", top:-34, width: 61, height: 61, backgroundColor:'white', borderRadius:100/2, flexDirection:'row', padding:10, alignItems:'center', justifyContent:'space-between', borderColor:'lightgray', borderWidth:1}}>
              <View style={{alignItems:'center', justifyContent:'center'}}>
                <TouchableOpacity onPress={()=>this.props.navigation.navigate("Maps")}>
                  <Image style={{height:40, width:40, resizeMode:'contain', textAlign:'center'}} source={require("../../assets/img/needs-detail/wishlist2.png")} />
                </TouchableOpacity>
              </View>
            </View>
          </View>

          <View style={{ justifyContent:'center', padding:15}}>
                <Text style={{width:'100%', fontSize:24, fontWeight:'bold', color:'black'}}>Parrot zik wireless noize hadphones with touch control </Text>
                <View style={{flexDirection:'row', alignItems:'center', marginTop:10}}>
                  <Rating
                    type="custom"
                    ratingColor="orange" 
                    fractions={1}
                    startingValue={4.5}
                    readonly
                    imageSize={15}
                  />
                  <Text style={{fontSize:15, color:'lightgray', marginLeft:5}}>4.5/5 (12 Tinjauan)</Text>
                </View>
                
          </View>
          
          <View style={{padding:10, flexDirection:'row', justifyContent:'space-between', alignItems:'center', paddingTop:2, paddingBottom:20}}>
            <View style={{flexDirection:'row', alignItems:'center'}}>
              <Text style={{marginLeft:5, fontWeight:'bold', fontSize:24, color:'#4593f9'}}>Rp. 2.500.000</Text>
            </View>
          </View> 


          <View style={{padding:10, paddingTop:1, paddingBottom:1}}>
            <View style={{borderColor:'lightgray', borderTopWidth:1}}>
              <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center', padding:15, paddingBottom:1}}>
              </View>
            </View>
            
            </View>
          


          <View style={{padding:15, paddingTop:1}}>
            <Text style={{marginTop:10, fontWeight:'bold', fontSize:16, color:'black'}}>Product Information</Text>
            <ScrollView horizontal={true} scrollEnabled={false} style={{marginTop:10}}>
              <View style={{padding:10, alignItems:'center', justifyContent:'center'}}>
                <View style={{alignItems:'center', justifyContent:'center', padding:20, backgroundColor:'#f4f7fc', borderRadius:5 }}>
                  <Image style={{height:35, width:35, resizeMode:'contain'}} source={require("../../assets/img/needs-detail/kondisi.png")}/>
                </View>
                <Text style={{marginTop:5, marginBottom:5}}>Kondisi</Text>
                <Text style={{marginTop:5, marginBottom:5, backgroundColor:'#4593f9', color:'white', fontWeight:'bold'}}>Baru</Text>
              </View>
              <View style={{padding:10, alignItems:'center', justifyContent:'center'}}>
                <View style={{alignItems:'center', justifyContent:'center', padding:20, backgroundColor:'#f4f7fc', borderRadius:5 }}>
                  <Image style={{height:35, width:35, resizeMode:'contain'}} source={require("../../assets/img/needs-detail/Dilihat.png")}/>
                </View>
                <Text style={{marginTop:5, marginBottom:5}}>Dilihat</Text>
                <Text style={{marginTop:5, marginBottom:5}}>20</Text>
              </View>
              <View style={{padding:10, alignItems:'center', justifyContent:'center'}}>
                <View style={{alignItems:'center', justifyContent:'center', padding:20, backgroundColor:'#f4f7fc', borderRadius:5 }}>
                  <Image style={{height:35, width:35, resizeMode:'contain'}} source={require("../../assets/img/needs-detail/Terjual.png")}/>
                </View>
                <Text style={{marginTop:5, marginBottom:5}}>Terjual</Text>
                <Text style={{marginTop:5, marginBottom:5}}>15</Text>
              </View>
              <View style={{padding:10, alignItems:'center', justifyContent:'center'}}>
                <View style={{alignItems:'center', justifyContent:'center', padding:20, backgroundColor:'#f4f7fc', borderRadius:5 }}>
                  <Image style={{height:35, width:35, resizeMode:'contain'}} source={require("../../assets/img/needs-detail/wishlist.png")}/>
                </View>
                <Text style={{marginTop:5, marginBottom:5}}>Wishlist</Text>
                <Text style={{marginTop:5, marginBottom:5}}>4</Text>
              </View>
            </ScrollView>
          </View>

          <View style={{padding:10}}>
            <View style={{borderColor:'lightgray', borderTopWidth:1, borderBottomWidth:1}}>
              <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center', padding:15, paddingBottom:15}}>
                <Text>Condition</Text>
                <Text style={{marginLeft:5, fontWeight:'bold', color:'black'}}>New With Box</Text>
              </View>
            </View>
            <View style={{borderColor:'lightgray', borderBottomWidth:1}}>
              <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center', padding:15, paddingBottom:15}}>
                <Text>Brand</Text>
                <Text style={{marginLeft:5, fontWeight:'bold', color:'black'}}>Sony</Text>
              </View>
            </View>
            <View style={{borderColor:'lightgray', borderBottomWidth:1}}>
              <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center', padding:15, paddingBottom:15}}>
                <Text>Weight</Text>
                <Text style={{marginLeft:5, fontWeight:'bold', color:'black'}}>950 grams</Text>
              </View>
            </View>
            <View style={{borderColor:'lightgray'}}>
              <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center', padding:15, paddingBottom:15}}>
                <Text>Style</Text>
                <Text style={{marginLeft:5, fontWeight:'bold', color:'black'}}>Basketball Shoes</Text>
              </View>
            </View>
          </View>

          <View style={{padding:15}}>
            <Text style={{marginTop:10, fontWeight:'bold', fontSize:16, color:'black'}}>Description</Text>
            <Text style={{marginTop:10}}>E universalmente riconosciuto che un lettore che osserva il layout di una pagina viene distratto dal contenuto testuale se questo e leggibile. Lo scopo dell’utilizzo del Lorem Ipsum e che offre una nomale distribuzione delle lettere (al contrario di quanto avviene se si utilizzano brevi frasi ripetute, ad esempio “testo qui”), apparendo come un nomale blocco di testo leggibile. {'\n \n'}Molti software di impaginazione e di web design utilizzano Lorem Ipsum come testo modello. Molte versioni del testo sono state prodotte negli anni, a volte casualmente, a volte di proposito (ad esempio inserendo passiggi ironici).</Text>
          </View>

          

          <View style={{backgroundColor:'#EAF2FF', marginTop:20}}>
            <View style={{height:50, width:'100%', flexDirection:'row'}}>
              <View style={{width:'75%', height:'100%', alignItems:'center', flexDirection:'row', paddingLeft:20}}>
                <Image style={{width:'15%', height:'40%', resizeMode:'contain'}} source={require('../../assets/img/home/crown.png')}/>
                <Text style={{fontSize:18, fontWeight:'bold', color:'black', marginLeft:5}}>Other Products</Text>
              </View>
            </View>
            <ScrollView horizontal={true} style={{height:225, width:'100%', flexDirection:'row', marginTop:10, marginBottom:10}}>
              <TouchableOpacity style={{height:'100%', width:Dimensions.get('window').width*0.45, padding:10, alignItems:'center'}}>
                <Image style={{height:'70%', width:'95%', borderRadius:10}} source={require("../../assets/img/home/best-selling-1.png")}/>
                <Text style={{width:'93%', marginTop:5, color:'black'}}>Al contrario di pensi quuanto ...</Text>
                <Text style={{width:'93%', marginTop:10, color:'#65c5f2'}}>Rp. 12.000</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{height:'100%', width:Dimensions.get('window').width*0.45, padding:10, alignItems:'center'}}>
                <Image style={{height:'70%', width:'95%', borderRadius:10}} source={require("../../assets/img/home/best-selling-2.png")}/>
                <Text style={{width:'93%', marginTop:5, color:'black'}}>Al contrario di pensi quuanto ...</Text>
                <Text style={{width:'93%', marginTop:10, color:'#65c5f2'}}>Rp. 12.000</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{height:'100%', width:Dimensions.get('window').width*0.45, padding:10, alignItems:'center'}}>
                <Image style={{height:'70%', width:'95%', borderRadius:10}} source={require("../../assets/img/home/best-selling-2.png")}/>
                <Text style={{width:'93%', marginTop:5, color:'black'}}>Al contrario di pensi quuanto ...</Text>
                <Text style={{width:'93%', marginTop:10, color:'#65c5f2'}}>Rp. 12.000</Text>
              </TouchableOpacity>
            </ScrollView>
          </View>
        
          <View style={{marginTop:1, flexDirection:'row'}}>
            <TouchableOpacity style={{alignItems:'center', justifyContent:'center', width:'20%', backgroundColor:'#cecece'}}>
              <Entypo name={"chat"} size={30} color={"white"}/>
            </TouchableOpacity>
            <TouchableOpacity style={{alignItems:'center', justifyContent:'center', width:'80%', flexDirection:'row', backgroundColor:'#65c5f2', padding:15}}>
              <Text style={{color:'white', fontWeight:'bold', fontSize:18}}>Buy Now</Text>
            </TouchableOpacity>
          </View>
          
        </ScrollView>
        <Maintabs
          navigate={this.props.navigation.navigate}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#eaf2ff',
  },
  content:{
    flex:1,
    width:'100%',
  },
  imgUser:{
    width: 130,
    height: 130,
    borderRadius: 65
  },
});

const mapStateToProps = (state) => {
    return {
        data: state.loginReducer.data,
        isFetching: state.loginReducer.isFetching,
        error: state.loginReducer.error,
        message: state.loginReducer.message,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {

    };
};

export default connect(mapStateToProps,mapDispatchToProps)(NeedsDetail);
