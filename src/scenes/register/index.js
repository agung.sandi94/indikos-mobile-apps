import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image, TouchableOpacity, TextInput, ScrollView, Dimensions} from 'react-native';
import { connect } from 'react-redux';
import { styles } from "../../components/stylesheet";
import Picker from 'react-native-picker';
import FontAwesome from 'react-native-vector-icons/dist/FontAwesome';

class Register extends Component {
  static navigationOptions = {
    header: null,
    headerBackTitle: 'Back'
  };

  constructor(props) {
    super(props);
    this._showClassPicker = this._showClassPicker.bind(this);
    this.state = {
      name: "",
      title1: "",
      title2: "",
      email: "",
      password: "",
      confirmPassword: "",
      type: "Tipe Pengguna",
      hidePassword:true,
      hideRetypePassword:true,
    };
  }

  _showClassPicker() {
      Picker.init({
          pickerData: ['Member','Agen','Pemilik'],
          selectedValue: ['Member'],
          pickerTitleText:"Tipe Pengguna",
          onPickerConfirm: pickedValue => {
              this.setState({
                  type:pickedValue[0]
              })
          },
          onPickerCancel: pickedValue => {
              console.log( pickedValue);
          },
          onPickerSelect: pickedValue => {
              console.log( pickedValue);

          }
      });
      Picker.show();
  }

  render() {
    return (
      <ScrollView style={styleses.container}>
        <TouchableOpacity style={{position:'absolute', top:25, left:25}} onPress={()=>this.props.navigation.goBack()}>
          <FontAwesome name={"angle-left"} color={'gray'} size={40} />
        </TouchableOpacity>
        <View style={{width:'100%', justifyContent:'center', alignItems:'center', marginTop:50, marginBottom:30}}>
          <Image style={{height:Dimensions.get('window').height*0.25, height:Dimensions.get('window').width*0.4, resizeMode:'contain'}} source={require('../../assets/img/logo.png')} />
          <Text style={styles.title}>Create Account</Text>
          <Text style={[styles.header,{textAlign:'center', marginLeft:25, marginRight:25}]}>Please sign up to your personal account if you want to use all our premium products</Text>
          <TextInput
            ref={(input) => { this.firstTextInput = input; }}
            style={styles.textInput}
            placeholder={"Nama Depan"}
            onChangeText={(firstname) => this.setState({firstname})}
            value={this.state.firstname}
            onSubmitEditing={() => { this.secondTextInput.focus(); }}
            blurOnSubmit={false}
            returnKeyType = { "next" }
          />
          <TextInput
            ref={(input) => { this.secondTextInput = input; }}
            style={[styles.textInput,{marginTop:10}]}
            placeholder={"Nama Belakang"}
            onChangeText={(lastname) => this.setState({lastname})}
            value={this.state.lastname}
            onSubmitEditing={() => { this.thirdTextInput.focus(); }}
            blurOnSubmit={false}
            returnKeyType = { "next" }
          />
          <TextInput
          ref={(input) => { this.thirdTextInput = input; }}
            style={[styles.textInput,{marginTop:10}]}
            placeholder={"Email"}
            onChangeText={(email) => this.setState({email})}
            value={this.state.email}
            keyboardType={'email-address'}
            onSubmitEditing={() => { this.fourthTextInput.focus(); }}
            blurOnSubmit={false}
            returnKeyType = { "next" }
          />
          <TextInput
            ref={(input) => { this.fourthTextInput = input; }}
            style={[styles.textInput,{marginTop:10}]}
            placeholder={"Nomor Handphone"}
            onChangeText={(phonenumber) => this.setState({phonenumber})}
            value={this.state.phonenumber}
            keyboardType={'phone-pad'}
            onSubmitEditing={() => { this.fifthTextInput.focus(); }}
            blurOnSubmit={false}
            returnKeyType = { "next" }
          />
          <TouchableOpacity style={[styles.textInput,{marginTop:10, flexDirection:'row', justifyContent:'space-between', alignItems:'center'}]} onPress={this._showClassPicker}>
            <Text>{this.state.type}</Text>
            <FontAwesome name={"caret-down"} color={'#65c5f2'} size={20} />
          </TouchableOpacity>
          <View style={[styles.textInput,{marginTop:10, paddingLeft:0, flexDirection:'row', alignItems:'center', justifyContent:'space-between'}]}>
            <TextInput
              ref={(input) => { this.sixthTextInput = input; }}
              style={{width:'90%', paddingLeft:20}}
              placeholder={"Password"}
              onChangeText={(password) => this.setState({password})}
              value={this.state.password}
              secureTextEntry={this.state.hidePassword}
              onSubmitEditing={() => { this.seventhTextInput.focus(); }}
              blurOnSubmit={false}
              returnKeyType = { "next" }
            />
            <TouchableOpacity onPress={()=>this.setState({hidePassword:!this.state.hidePassword})}>
              <Image style={{width:20,height:20,resizeMode:'contain'}} source={require("../../assets/img/show-password.png")} />
            </TouchableOpacity>
          </View>
          <View style={[styles.textInput,{marginTop:10, paddingLeft:0, flexDirection:'row', alignItems:'center', justifyContent:'space-between'}]}>
            <TextInput
              ref={(input) => { this.seventhTextInput = input; }}
              style={{width:'90%', paddingLeft:20}}
              placeholder={"Retype Password"}
              onChangeText={(retypepassword) => this.setState({retypepassword})}
              value={this.state.retypepassword}
              secureTextEntry={this.state.hideRetypePassword}
            />
            <TouchableOpacity onPress={()=>this.setState({hideRetypePassword:!this.state.hideRetypePassword})}>
              <Image style={{width:20,height:20,resizeMode:'contain'}} source={require("../../assets/img/show-password.png")} />
            </TouchableOpacity>
          </View>
          <TouchableOpacity style={styles.btnBlue} onPress={()=>this.props.navigation.navigate("Verification")}>
            <Text style={styles.textBtnBlue}>Create</Text>
          </TouchableOpacity>
          <View style={{marginTop:15, flexDirection:'row'}}>
            <Text style={[styles.header,{marginTop:0}]}>Do you have an account?</Text>
            <TouchableOpacity style={styles.hyperlink} onPress={()=>this.props.navigation.navigate('Login')}>
              <Text style={styles.hyperlinkText}>Sign In here</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styleses = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  }
});

const mapStateToProps = (state) => {
    return {
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
    };
};

export default connect(mapStateToProps,mapDispatchToProps)(Register);
