import React, {Component} from 'react';
import { Platform, StyleSheet, Text, View, Dimensions, ScrollView, Image, TextInput, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { finishCheck } from '../login/actions'
import Maintabs from '../../components/maintabs';
import FontAwesome from 'react-native-vector-icons/dist/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/dist/FontAwesome5';
import Ionicons from 'react-native-vector-icons/dist/Ionicons';
import Octicons from 'react-native-vector-icons/dist/Octicons';
import Picker from 'react-native-picker';
import Slider from "react-native-slider";

class IndikosSearch extends Component {
  static navigationOptions = {
    header: null,
    headerBackTitle: 'Back'
  };

  constructor(props) {
    super(props);
    this._showTypePicker = this._showTypePicker.bind(this);
    this._showDurationPicker = this._showDurationPicker.bind(this);
    this.state = {
      text: "",
      activeTab:1,
      type: "Men",
      duration: "Per Year",
      value:250000
    }
  }

  _showTypePicker() {
      Picker.init({
          pickerData: ['Men','Woman'],
          selectedValue: ['Men'],
          pickerTitleText:"Men",
          onPickerConfirm: pickedValue => {
              this.setState({
                  type:pickedValue
              })
          },
          onPickerCancel: pickedValue => {
              console.log( pickedValue);
          },
          onPickerSelect: pickedValue => {
              console.log( pickedValue);

          }
      });
      Picker.show();
  }

  _showDurationPicker() {
      Picker.init({
          pickerData: ['Per Year','Per Month','Per Day'],
          selectedValue: ['Per Year'],
          pickerTitleText:"Per Year",
          onPickerConfirm: pickedValue => {
              this.setState({
                  duration:pickedValue[0]
              })
          },
          onPickerCancel: pickedValue => {
              console.log( pickedValue);
          },
          onPickerSelect: pickedValue => {
              console.log( pickedValue);

          }
      });
      Picker.show();
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={{height:'100%', width:'100%', marginBottom:60, backgroundColor:'white'}}>
          <Image style={[styles.oval,{resizeMode:'cover', backgroundColor:'transparent'}]} source={{uri:"https://cdn-asset.hipwee.com/wp-content/uploads/2017/10/hipwee-7-alasan-harus-punya-bisnis-kos-kosan-ZwmciGeY7s-2-750x419.jpg"}} />
          <View style={[styles.oval,{borderColor:'#65c5f2', borderWidth:2}]} />
          <View style={{padding:20}}>
            <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center', padding:10}}>
              <View>
                <Text style={{color:'white', fontWeight:'bold', fontSize:24}}>Hi, Lauren</Text>
                <Text style={{color:'white'}}>It's nice day to find kosan</Text>
              </View>
              <FontAwesome name={"user-circle-o"} size={35} color={"white"} />
            </View>
            <View style={{marginTop:20, padding:10}}>
              <Text style={{color:'white', fontWeight:'bold', fontSize:18}}>Select location</Text>
              <View style={{flexDirection:'row', borderBottomColor:'white', borderBottomWidth:1.5, justifyContent:'space-between', alignItems:'flex-end'}}>
                <TextInput
                  style={{color:'white', width:'90%', height:35, paddingBottom:0}}
                  onChangeText={(text) => this.setState({text})}
                  placeholderTextColor={"white"}
                  placeholder={"Search"}
                  value={this.state.text}
                />
                <Ionicons name={"md-search"} size={30} color={"white"} />
              </View>
            </View>
            <View style={{marginTop:10, padding:10, flexDirection:'row'}}>
              <View style={{width:'50%', paddingRight:5}}>
                <Text style={{color:'white', fontWeight:'bold', fontSize:16}}>Kosan type</Text>
                <TouchableOpacity style={{flexDirection:'row', borderBottomColor:'white', borderBottomWidth:1.5, justifyContent:'space-between', alignItems:'flex-end', padding:5, height:40}} onPress={this._showTypePicker}>
                  <Ionicons name={(this.state.type=="Men")?"ios-man":"ios-woman"} size={20} color={"white"} />
                  <Text style={{color:'white', width:'60%'}}>{this.state.type}</Text>
                  <Ionicons name={"ios-arrow-down"} size={15} color={"white"} />
                </TouchableOpacity>
              </View>
              <View style={{width:'50%', paddingLeft:5}}>
                <Text style={{color:'white', fontWeight:'bold', fontSize:16}}>Duration</Text>
                <TouchableOpacity style={{flexDirection:'row', borderBottomColor:'white', borderBottomWidth:1.5, justifyContent:'space-between', alignItems:'flex-end', padding:5, height:40}} onPress={this._showDurationPicker}>
                  <FontAwesome name={"hourglass"} size={20} color={"white"} />
                  <Text style={{color:'white', width:'60%'}}>{this.state.duration}</Text>
                  <Ionicons name={"ios-arrow-down"} size={15} color={"white"} />
                </TouchableOpacity>
              </View>
            </View>
            <View style={{marginTop:20, padding:10}}>
              <Text style={{color:'white', fontWeight:'bold', fontSize:16}}>Price range</Text>
              <Slider
                value={this.state.value}
                maximumTrackTintColor={"white"}
                maximumValue={500000}
                minimumTrackTintColor={"darkblue"}
                minimumValue={0}
                step={125000}
                thumbStyle={{backgroundColor:'transparent', alignItems:'center', justifyContent:'center', borderRadius:0}}
                thumbImage={require("../../assets/img/indikos-search/thumb.png")}
                style={{zIndex:1}}
                onValueChange={value => this.setState({ value })}
              />
              <View style={{flexDirection:'row', justifyContent:'space-between', width:'100%', marginTop:-25, zIndex:0}}>
                <View style={{backgroundColor:(this.state.value > 0)?'darkblue':'white', height:10, width:10, borderRadius:5}}></View>
                <View style={{backgroundColor:(this.state.value > 125000)?'darkblue':'white', height:10, width:10, borderRadius:5}}></View>
                <View style={{backgroundColor:(this.state.value > 250000)?'darkblue':'white', height:10, width:10, borderRadius:5}}></View>
                <View style={{backgroundColor:(this.state.value > 375000)?'darkblue':'white', height:10, width:10, borderRadius:5}}></View>
                <View style={{backgroundColor:(this.state.value > 500000)?'darkblue':'white', height:10, width:10, borderRadius:5}}></View>
              </View>
              <View style={{flexDirection:'row', justifyContent:'space-between', width:'100%', marginTop:5}}>
                <Text style={{color:'white', fontSize:16, fontWeight:'bold'}}>0</Text>
                <Text style={{color:'white', fontSize:16, fontWeight:'bold'}}>Rp 500.000</Text>
              </View>
            </View>
            <TouchableOpacity style={{alignItems:'center', justifyContent:'center', backgroundColor:'white', marginTop:20, borderRadius:10, height:50}}>
              <Text style={{color:'#65c5f2', fontWeight:'bold', fontSize:16}}>Search</Text>
            </TouchableOpacity>
          </View>
          <View style={{alignItems:'center', flexDirection:'row', padding:20, paddingTop:0, paddingBottom:0, marginTop:'50%'}}>
            <Image style={{width:30, height:30, resizeMode:'contain'}} source={require('../../assets/img/indikos-search/title-icon.png')}/>
            <Text style={{fontSize:18, fontWeight:'bold', color:'black', marginLeft:10}}>Search results kosan</Text>
          </View>
          <View style={{padding:20}}>
            <ScrollView horizontal={true} style={{borderTopColor:'lightgray', borderBottomColor:'lightgray', borderTopWidth:1, borderBottomWidth:1}}>
              <TouchableOpacity onPress={()=> this.setState({activeTab:1})} style={{borderTopColor:'lightgray', borderBottomColor:(this.state.activeTab==1)?'#65c5f2':'lightgray', borderBottomWidth:(this.state.activeTab==1)?2:0, alignItems:'center', justifyContent:'center', padding:10, paddingLeft:0}}>
                <Text style={{color:(this.state.activeTab==1)?'#65c5f2':'black', fontWeight:(this.state.activeTab==1)?'bold':'500'}}>For you</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=> this.setState({activeTab:2})} style={{borderTopColor:'lightgray', borderBottomColor:(this.state.activeTab==2)?'#65c5f2':'lightgray', borderBottomWidth:(this.state.activeTab==2)?2:0, alignItems:'center', justifyContent:'center', padding:10, paddingLeft:0}}>
                <Text style={{color:(this.state.activeTab==2)?'#65c5f2':'black', fontWeight:(this.state.activeTab==2)?'bold':'500'}}>Popular</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=> this.setState({activeTab:3})} style={{borderTopColor:'lightgray', borderBottomColor:(this.state.activeTab==3)?'#65c5f2':'lightgray', borderBottomWidth:(this.state.activeTab==3)?2:0, alignItems:'center', justifyContent:'center', padding:10, paddingLeft:0}}>
                <Text style={{color:(this.state.activeTab==3)?'#65c5f2':'black', fontWeight:(this.state.activeTab==3)?'bold':'500'}}>Inexpensive</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=> this.setState({activeTab:4})} style={{borderTopColor:'lightgray', borderBottomColor:(this.state.activeTab==4)?'#65c5f2':'lightgray', borderBottomWidth:(this.state.activeTab==4)?2:0, alignItems:'center', justifyContent:'center', padding:10, paddingLeft:0}}>
                <Text style={{color:(this.state.activeTab==4)?'#65c5f2':'black', fontWeight:(this.state.activeTab==4)?'bold':'500'}}>Standard</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=> this.setState({activeTab:5})} style={{borderTopColor:'lightgray', borderBottomColor:(this.state.activeTab==5)?'#65c5f2':'lightgray', borderBottomWidth:(this.state.activeTab==5)?2:0, alignItems:'center', justifyContent:'center', padding:10, paddingLeft:0}}>
                <Text style={{color:(this.state.activeTab==5)?'#65c5f2':'black', fontWeight:(this.state.activeTab==5)?'bold':'500'}}>Expensive</Text>
              </TouchableOpacity>
            </ScrollView>
          </View>
          <View style={{padding:20}}>
            <TouchableOpacity onPress={()=>this.props.navigation.navigate('KosanDetail')} style={{marginTop:30, marginBottom:30, height:200, borderRadius:10, borderColor:'lightgray', borderWidth:1, flexDirection:'row', justifyContent:'space-between'}}>
              <View style={{height:'100%', width:'40%'}}>
                <View style={{position:'absolute', bottom:0, height:'115%', width:'100%', zIndex:1}}>
                  <Image style={{height:'100%', width:'100%', borderRadius:10, borderBottomRightRadius:0}} source={require("../../assets/img/indikos-search/kosan-1.png")}/>
                  <View style={{position:'absolute', bottom: 0, left: '10%', width:'80%', height:'18%', backgroundColor:'#65c5f2', alignItems:'center', justifyContent:'center', borderTopLeftRadius: 5, borderTopRightRadius: 5}}>
                    <Text style={{color:'white', fontWeight:'bold'}}>Khusus Putra</Text>
                  </View>
                </View>
              </View>
              <View style={{height:'100%', width:'60%', padding:10}}>
                <View style={{height:'70%', borderBottomColor:'lightgray', borderBottomWidth:1}}>
                  <Text style={{fontWeight:'bold', color:'black', fontSize:16, marginTop:5}}>Kosan Blockchain</Text>
                  <View style={{flexDirection:'row', marginTop:5}}>
                    <Octicons name={"location"} size={14} color={"#65c5f2"}/>
                    <Text style={{fontSize:12, marginLeft:5, marginRight:5}}>Jl. Lorem Ipsum e un testo tempo segnaposto utilizzato ...</Text>
                  </View>
                  <ScrollView horizontal={true} style={{marginTop:10, marginBottom:10}}>
                    <View style={{alignItems:'center', justifyContent:'center'}}>
                      <Image style={{height:20, width:20, resizeMode:'contain'}} source={require("../../assets/img/kosan-detail/facilities/fan.png")}/>
                    </View>
                    <View style={{alignItems:'center', justifyContent:'center', marginLeft:10}}>
                      <Image style={{height:20, width:20, resizeMode:'contain'}} source={require("../../assets/img/kosan-detail/facilities/security.png")}/>
                    </View>
                    <View style={{alignItems:'center', justifyContent:'center', marginLeft:10}}>
                      <Image style={{height:20, width:20, resizeMode:'contain'}} source={require("../../assets/img/kosan-detail/facilities/bed.png")}/>
                    </View>
                    <View style={{alignItems:'center', justifyContent:'center', marginLeft:10}}>
                      <Image style={{height:20, width:20, resizeMode:'contain'}} source={require("../../assets/img/kosan-detail/facilities/wardrobe.png")}/>
                    </View>
                  </ScrollView>
                </View>
                <View style={{height:'30%', flexDirection:'row', alignItems:'center', justifyContent:'space-between', paddingTop:10}}>
                  <Text style={{color:'#65c5f2', fontWeight:'800'}}>Rp 500.000</Text>
                  <TouchableOpacity style={{padding:10, borderRadius:20, borderColor:'#65c5f2', borderWidth:1, alignItems:'center', justifyContent:'center'}}>
                    <Text style={{color:'#65c5f2', fontWeight:'500'}}>Book Now</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>this.props.navigation.navigate('KosanDetail')} style={{marginTop:30, marginBottom:30, height:200, borderRadius:10, borderColor:'lightgray', borderWidth:1, flexDirection:'row', justifyContent:'space-between'}}>
              <View style={{height:'100%', width:'40%'}}>
                <View style={{position:'absolute', bottom:0, height:'115%', width:'100%', zIndex:1}}>
                  <Image style={{height:'100%', width:'100%', borderRadius:10, borderBottomRightRadius:0}} source={require("../../assets/img/indikos-search/kosan-2.png")}/>
                  <View style={{position:'absolute', bottom: 0, left: '10%', width:'80%', height:'18%', backgroundColor:'#65c5f2', alignItems:'center', justifyContent:'center', borderTopLeftRadius: 5, borderTopRightRadius: 5}}>
                    <Text style={{color:'white', fontWeight:'bold'}}>Khusus Putri</Text>
                  </View>
                </View>
              </View>
              <View style={{height:'100%', width:'60%', padding:10}}>
                <View style={{height:'70%', borderBottomColor:'lightgray', borderBottomWidth:1}}>
                  <Text style={{fontWeight:'bold', color:'black', fontSize:16, marginTop:5}}>Kosan Blockchain</Text>
                  <View style={{flexDirection:'row', marginTop:5}}>
                    <Octicons name={"location"} size={14} color={"#65c5f2"}/>
                    <Text style={{fontSize:12, marginLeft:5, marginRight:5}}>Jl. Lorem Ipsum e un testo tempo segnaposto utilizzato ...</Text>
                  </View>
                  <ScrollView horizontal={true} style={{marginTop:10, marginBottom:10}}>
                    <View style={{alignItems:'center', justifyContent:'center'}}>
                      <Image style={{height:20, width:20, resizeMode:'contain'}} source={require("../../assets/img/kosan-detail/facilities/fan.png")}/>
                    </View>
                    <View style={{alignItems:'center', justifyContent:'center', marginLeft:10}}>
                      <Image style={{height:20, width:20, resizeMode:'contain'}} source={require("../../assets/img/kosan-detail/facilities/security.png")}/>
                    </View>
                    <View style={{alignItems:'center', justifyContent:'center', marginLeft:10}}>
                      <Image style={{height:20, width:20, resizeMode:'contain'}} source={require("../../assets/img/kosan-detail/facilities/bed.png")}/>
                    </View>
                    <View style={{alignItems:'center', justifyContent:'center', marginLeft:10}}>
                      <Image style={{height:20, width:20, resizeMode:'contain'}} source={require("../../assets/img/kosan-detail/facilities/wardrobe.png")}/>
                    </View>
                  </ScrollView>
                </View>
                <View style={{height:'30%', flexDirection:'row', alignItems:'center', justifyContent:'space-between', paddingTop:10}}>
                  <Text style={{color:'#65c5f2', fontWeight:'800'}}>Rp 500.000</Text>
                  <TouchableOpacity style={{padding:10, borderRadius:20, borderColor:'#65c5f2', borderWidth:1, alignItems:'center', justifyContent:'center'}}>
                    <Text style={{color:'#65c5f2', fontWeight:'500'}}>Book Now</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </ScrollView>
        <Maintabs
          navigate={this.props.navigation.navigate}
        />
      </View>
    );
  }
}

const styles= StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#eaf2ff',
  },
  oval: {
      height: Dimensions.get('window').width * 2,
      width: Dimensions.get('window').width * 3,
      marginLeft: -(Dimensions.get('window').width),
      borderBottomLeftRadius: Dimensions.get('window').width*1.5,
      borderBottomRightRadius: Dimensions.get('window').width*1.5,
      backgroundColor: 'rgba(26, 175, 244, 0.75)',
      position: 'absolute',
      top: -(Dimensions.get('window').width*0.4),
  },
});

const mapStateToProps = (state) => {
    return {
        data: state.loginReducer.data,
        isFetching: state.loginReducer.isFetching,
        error: state.loginReducer.error,
        message: state.loginReducer.message,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        finishCheck: () => dispatch(finishCheck()),
    };
};

export default connect(mapStateToProps,mapDispatchToProps)(IndikosSearch);
