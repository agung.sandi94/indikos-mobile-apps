import React from 'react';
import AppNavigator from './AppNavigator';
import { connect } from "react-redux";

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      token:'',
      user:[]
    }
  }

  render() {
    return (
      <AppNavigator/>
    );
  }
}

const mapStateToProps = state => ({
});

export default connect(mapStateToProps)(App);
