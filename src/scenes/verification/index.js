/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image, TouchableOpacity, TextInput, ScrollView, Dimensions} from 'react-native';
import { connect } from 'react-redux';
import { styles } from "../../components/stylesheet";
import FontAwesome from 'react-native-vector-icons/dist/FontAwesome';

class Verification extends Component {
  static navigationOptions = {
    header: null,
    headerBackTitle: 'Back'
  };

  constructor(props) {
    super(props);
    this.state = {
      firstcode: "",
      secondcode: "",
      thirdcode: "",
      fourthcode: "",
    };
  }

  render() {
    return (
      <View style={[styles.container,{justifyContent:'flex-start'}]}>
        <TouchableOpacity style={{position:'absolute', top:25, left:25}} onPress={()=>this.props.navigation.goBack()}>
          <FontAwesome name={"angle-left"} color={'gray'} size={40} />
        </TouchableOpacity>
        <Text style={[styles.title,{marginTop:50}]}>Verify Phone</Text>
        <Text style={[styles.header,{textAlign:'center', marginLeft:75, marginRight:75, fontSize:16}]}>We sent you a code to verify your phone number</Text>
        <Text style={[styles.header,{color:'lightgray'}]}>Sent to (012) 123-456-789</Text>
        <View style={{width:'100%', height:'25%', flexDirection:'row', alignItems:'center', justifyContent:'space-between', paddingLeft:30, paddingRight:30}}>
          <TextInput
            ref={(input) => { this.firstTextInput = input; }}
            style={{backgroundColor:'white', borderColor:'gray', borderWidth:1, borderRadius:2, width:'20%', height:75, fontWeight:'bold', fontSize:24, textAlign:'center'}}
            onChangeText={(firstcode) => {
              if(firstcode.length<2){
                this.setState({firstcode});
                if(firstcode.length==1){
                  this.secondTextInput.focus();
                }
              }
            }}
            value={this.state.firstcode}
            onSubmitEditing={() => { this.secondTextInput.focus(); }}
            blurOnSubmit={false}
            autoFocus={true}
            keyboardType={'numeric'}
            returnKeyType = { "next" }
          />
          <TextInput
            ref={(input) => { this.secondTextInput = input; }}
            style={{backgroundColor:'white', borderColor:'gray', borderWidth:1, borderRadius:2, width:'20%', height:75, fontWeight:'bold', fontSize:24, textAlign:'center'}}
            onKeyPress={({ nativeEvent }) => {
              if (this.state.secondcode==""){
                if (nativeEvent.key === 'Backspace') {
                  this.setState({firstcode:""});
                  this.firstTextInput.focus();
                }
              }
            }}
            onChangeText={(secondcode) => {
              if(secondcode.length<2){
                this.setState({secondcode})
                if(secondcode.length==1){
                  this.thirdTextInput.focus();
                } else if (secondcode.length==0){
                  this.firstTextInput.focus();
                }
              }
            }}
            value={this.state.secondcode}
            onSubmitEditing={() => { this.thirdTextInput.focus(); }}
            blurOnSubmit={false}
            keyboardType={'numeric'}
            returnKeyType = { "next" }
          />
          <TextInput
            ref={(input) => { this.thirdTextInput = input; }}
            style={{backgroundColor:'white', borderColor:'gray', borderWidth:1, borderRadius:2, width:'20%', height:75, fontWeight:'bold', fontSize:24, textAlign:'center'}}
            onKeyPress={({ nativeEvent }) => {
              if (this.state.thirdcode==""){
                if (nativeEvent.key === 'Backspace') {
                  this.setState({secondcode:""});
                  this.secondTextInput.focus();
                }
              }
            }}
            onChangeText={(thirdcode) => {
              if(thirdcode.length<2){
                this.setState({thirdcode})
                if(thirdcode.length==1){
                  this.fourthTextInput.focus();
                } else if (thirdcode.length==0){
                  this.secondTextInput.focus();
                }
              }
            }}
            value={this.state.thirdcode}
            onSubmitEditing={() => { this.fourthTextInput.focus(); }}
            blurOnSubmit={false}
            keyboardType={'numeric'}
            returnKeyType = { "next" }
          />
          <TextInput
            ref={(input) => { this.fourthTextInput = input; }}
            style={{backgroundColor:'white', borderColor:'gray', borderWidth:1, borderRadius:2, width:'20%', height:75, fontWeight:'bold', fontSize:24, textAlign:'center'}}
            onKeyPress={({ nativeEvent }) => {
              if (this.state.fourthcode==""){
                if (nativeEvent.key === 'Backspace') {
                  this.setState({thirdcode:""});
                  this.thirdTextInput.focus();
                }
              }
            }}
            onChangeText={(fourthcode) => {
              if (fourthcode.length<2){
                this.setState({fourthcode})
                if (fourthcode.length==1){
                  this.props.navigation.navigate("Home");
                } else if (fourthcode.length==0){
                  this.thirdTextInput.focus();
                }
              }
            }}
            value={this.state.fourthcode}
            blurOnSubmit={false}
            keyboardType={'numeric'}
          />
        </View>
        <Text style={[styles.header,{color:'lightgray'}]}>I didn't receive a code!</Text>
        <TouchableOpacity style={[styles.btnBlue,{marginTop:15}]}>
          <Text style={styles.textBtnBlue}>Resend</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styleses = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  }
});

const mapStateToProps = (state) => {
    return {
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
    };
};

export default connect(mapStateToProps,mapDispatchToProps)(Verification);
