import { LOGIN_STATE } from './actions'

const initialState = {
  data: null,
  dataFetched: false,
  isFetching: false,
  error: false,
  message: "",
}

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_STATE.FETCHING_DATA:
      return {
        ...state,
        data: [],
        isFetching: true,
        message: ""
      }
    case LOGIN_STATE.FETCHING_DATA_SUCCESS:
      return {
        ...state,
        isFetching: false,
        data: action.data,
        message: ""
      }
    case LOGIN_STATE.FETCHING_DATA_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
        message: action.message
      }
    case LOGIN_STATE.CHECKING_DATA_FINISH:
      return {
        ...state,
        isFetching: false,
        error: false,
        message: ""
      }
    default:
      return state
  }
}
