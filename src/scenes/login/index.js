import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image, TouchableOpacity, TextInput, Alert, ScrollView, FlatList, SectionList} from 'react-native';
import { connect } from 'react-redux';
import { login,finishCheck } from './actions'
import { styles } from "../../components/stylesheet";
import SplashScreen from 'react-native-smart-splash-screen';
import LoadingView from '../../components/loadingView';
import MaterialCommunityIcons from 'react-native-vector-icons/dist/MaterialCommunityIcons';

class Login extends Component {
  static navigationOptions = {
    header: null,
    headerBackTitle: 'Back'
  };

  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      remember:false,
      isLoading: true,
    };
  }

  componentDidMount () {
    //SplashScreen.close(SplashScreen.animationType.scale, 850, 500)
    setTimeout(() => {
      this.setState({isLoading:false},()=>{
        SplashScreen.close({
          animationType: SplashScreen.animationType.scale,
          duration: 850,
          delay: 500,
        })
      });
    }, 5000);
  }

  render() {
    return (
      <View style={styles.container}>
        <LoadingView visible={this.state.isLoading} />
        <Image style={styles.logo} source={require('../../assets/img/logo.png')} />
        <Text style={styles.title}>Login</Text>
        <TouchableOpacity style={{width:'80%', height:50, backgroundColor:'#3b5998', flexDirection:'row', alignItems:'center', justifyContent:'center', borderRadius:25, marginTop:20}}>
          <Image style={{width:25, height:25, resizeMode:'contain'}} source={require('../../assets/img/icon-fb.png')} />
          <Text style={{color:'white', fontWeight:'bold', marginLeft:10}}>Continue with Facebook</Text>
        </TouchableOpacity>
        <Text style={styles.header}>Please login to your account</Text>
        <TextInput
          ref={(input) => { this.firstTextInput = input; }}
          style={styles.textInput}
          placeholder={"Username"}
          onChangeText={(username) => this.setState({username})}
          value={this.state.username}
          onSubmitEditing={() => { this.secondTextInput.focus(); }}
          blurOnSubmit={false}
          returnKeyType = { "next" }
        />
        <TextInput
          ref={(input) => { this.secondTextInput = input; }}
          style={styles.textInput}
          placeholder={"Password"}
          onChangeText={(password) => this.setState({password})}
          value={this.state.password}
          secureTextEntry={true}
        />
        <View style={{marginTop:15, width:'80%', justifyContent:'flex-end', alignItems:'center', flexDirection:'row'}}>
          <TouchableOpacity onPress={()=>this.setState({remember:!this.state.remember})}>
            <MaterialCommunityIcons name={(this.state.remember)?"checkbox-marked-outline":"checkbox-blank-outline"} size={20} />
          </TouchableOpacity>
          <Text style={{marginLeft:5}}>Remember Me</Text>
        </View>
        <TouchableOpacity style={styles.btnBlue} onPress={()=> this.props.navigation.navigate("Home")}>
          <Text style={styles.textBtnBlue}>Enter</Text>
        </TouchableOpacity>
        <View style={{marginTop:15, flexDirection:'row'}}>
          <Text style={[styles.header,{marginTop:0}]}>Don't have an account?</Text>
          <TouchableOpacity style={styles.hyperlink} onPress={()=>this.props.navigation.navigate('Register')}>
            <Text style={styles.hyperlinkText}>Sign Up</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
    return {
        data: state.loginReducer.data,
        isFetching: state.loginReducer.isFetching,
        error: state.loginReducer.error,
        message: state.loginReducer.message,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        login: (username,password) => dispatch(login(username,password)),
        finishCheck: () => dispatch(finishCheck()),
    };
};

export default connect(mapStateToProps,mapDispatchToProps)(Login);
