import React, {Component} from 'react';
import { Platform, StyleSheet, Text, View, Dimensions, ScrollView, Image, TextInput, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { finishCheck } from '../login/actions'
import Maintabs from '../../components/maintabs';
import FontAwesome from 'react-native-vector-icons/dist/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/dist/FontAwesome5';
import Ionicons from 'react-native-vector-icons/dist/Ionicons';
import Octicons from 'react-native-vector-icons/dist/Octicons';
import Picker from 'react-native-picker';
import Slider from "react-native-slider";

class TransactionHistoryList extends Component {
  static navigationOptions = ({navigation}) => ({
    title: "Transaction History" ,
    headerBackTitle: 'Back'
  });

  constructor(props) {
    super(props);
    this._showTypePicker = this._showTypePicker.bind(this);
    this._showDurationPicker = this._showDurationPicker.bind(this);
    this.state = {
      text: "",
      activeTab:1,
      type: "Men",
      duration: "Per Year",
      value:250000
    }
  }

  _showTypePicker() {
      Picker.init({
          pickerData: ['Men','Woman'],
          selectedValue: ['Men'],
          pickerTitleText:"Men",
          onPickerConfirm: pickedValue => {
              this.setState({
                  type:pickedValue
              })
          },
          onPickerCancel: pickedValue => {
              console.log( pickedValue);
          },
          onPickerSelect: pickedValue => {
              console.log( pickedValue);

          }
      });
      Picker.show();
  }

  _showDurationPicker() {
      Picker.init({
          pickerData: ['Per Year','Per Month','Per Day'],
          selectedValue: ['Per Year'],
          pickerTitleText:"Per Year",
          onPickerConfirm: pickedValue => {
              this.setState({
                  duration:pickedValue[0]
              })
          },
          onPickerCancel: pickedValue => {
              console.log( pickedValue);
          },
          onPickerSelect: pickedValue => {
              console.log( pickedValue);

          }
      });
      Picker.show();
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={{height:'100%', width:'100%', marginBottom:60, backgroundColor:'white'}}>

          <View style={{padding:20}}>
            <ScrollView horizontal={true} style={{borderTopColor:'lightgray', borderBottomColor:'lightgray', borderTopWidth:1, borderBottomWidth:1}}>
              <TouchableOpacity onPress={()=> this.setState({activeTab:1})} style={{borderTopColor:'lightgray', borderBottomColor:(this.state.activeTab==1)?'#65c5f2':'lightgray', borderBottomWidth:(this.state.activeTab==1)?2:0, alignItems:'center', justifyContent:'center', padding:10, paddingLeft:20}}>
                <Text style={{color:(this.state.activeTab==1)?'#65c5f2':'black', fontWeight:(this.state.activeTab==1)?'bold':'500'}}>New</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=> this.setState({activeTab:2})} style={{borderTopColor:'lightgray', borderBottomColor:(this.state.activeTab==2)?'#65c5f2':'lightgray', borderBottomWidth:(this.state.activeTab==2)?2:0, alignItems:'center', justifyContent:'center', padding:10, paddingLeft:20}}>
                <Text style={{color:(this.state.activeTab==2)?'#65c5f2':'black', fontWeight:(this.state.activeTab==2)?'bold':'500'}}>Pending</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=> this.setState({activeTab:3})} style={{borderTopColor:'lightgray', borderBottomColor:(this.state.activeTab==3)?'#65c5f2':'lightgray', borderBottomWidth:(this.state.activeTab==3)?2:0, alignItems:'center', justifyContent:'center', padding:10, paddingLeft:20}}>
                <Text style={{color:(this.state.activeTab==3)?'#65c5f2':'black', fontWeight:(this.state.activeTab==3)?'bold':'500'}}>Done</Text>
              </TouchableOpacity>
            </ScrollView>
          </View>
          <View style={{padding:20}}>
            <TouchableOpacity onPress={()=>this.props.navigation.navigate('KosanDetail')} style={{marginTop:30, marginBottom:30, height:200, borderRadius:10, borderColor:'lightgray', borderWidth:1, flexDirection:'row', justifyContent:'space-between'}}>
              <View style={{height:'100%', width:'40%'}}>
                <View style={{position:'absolute', bottom:0, height:'115%', width:'100%', zIndex:1}}>
                  <Image style={{height:'100%', width:'100%', borderRadius:10, borderBottomRightRadius:0}} source={require("../../assets/img/indikos-search/kosan-1.png")}/>
                  <View style={{position:'absolute', bottom: 0, left: '10%', width:'80%', height:'18%', backgroundColor:'#65c5f2', alignItems:'center', justifyContent:'center', borderTopLeftRadius: 5, borderTopRightRadius: 5}}>
                    <Text style={{color:'white', fontWeight:'bold'}}>Tertunda</Text>
                  </View>
                </View>
              </View>
              <View style={{height:'100%', width:'60%', padding:10}}>
                <View style={{height:'70%', borderBottomColor:'lightgray', borderBottomWidth:1}}>
                  <Text style={{fontWeight:'bold', color:'black', fontSize:16, marginTop:5}}>Kosan Blockchain</Text>
                  <View style={{flexDirection:'row', marginTop:5}}>
                    <Text style={{fontSize:12, marginLeft:1, marginRight:1}}>Durasi : </Text>
                    <Text style={{fontSize:12, marginLeft:1, marginRight:1}}>1 Bulan</Text>
                  </View>
                  <View style={{flexDirection:'row', marginTop:5}}>
                    <Text style={{fontSize:12, marginLeft:1, marginRight:1}}>Start Date : </Text>
                    <Text style={{fontSize:12, marginLeft:1, marginRight:1}}>Kamis, 1 Januari 2019</Text>
                  </View>
                  <View style={{flexDirection:'row', marginTop:5}}>
                    <Text style={{fontSize:12, marginLeft:1, marginRight:1}}>Status Pembayaran : </Text>
                    <Text style={{fontSize:12, marginLeft:1, marginRight:1}}>Belum Dibayar</Text>
                  </View>
                </View>
                <View style={{height:'30%', flexDirection:'row', alignItems:'center', justifyContent:'space-between', paddingTop:10}}>
                  <Text style={{color:'#65c5f2', fontWeight:'800'}}>Indikos</Text>
                  <TouchableOpacity style={{padding:10, borderRadius:20, borderColor:'#65c5f2', borderWidth:1, alignItems:'center', justifyContent:'center'}}>
                    <Text style={{color:'#65c5f2', fontWeight:'500'}}>   Lihat   </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>this.props.navigation.navigate('KosanDetail')} style={{marginTop:30, marginBottom:30, height:200, borderRadius:10, borderColor:'lightgray', borderWidth:1, flexDirection:'row', justifyContent:'space-between'}}>
              <View style={{height:'100%', width:'40%'}}>
                <View style={{position:'absolute', bottom:0, height:'115%', width:'100%', zIndex:1}}>
                  <Image style={{height:'100%', width:'100%', borderRadius:10, borderBottomRightRadius:0}} source={require("../../assets/img/indikos-search/kosan-1.png")}/>
                  <View style={{position:'absolute', bottom: 0, left: '10%', width:'80%', height:'18%', backgroundColor:'#65c5f2', alignItems:'center', justifyContent:'center', borderTopLeftRadius: 5, borderTopRightRadius: 5}}>
                    <Text style={{color:'white', fontWeight:'bold'}}>Tertunda</Text>
                  </View>
                </View>
              </View>
              <View style={{height:'100%', width:'60%', padding:10}}>
                <View style={{height:'70%', borderBottomColor:'lightgray', borderBottomWidth:1}}>
                  <Text style={{fontWeight:'bold', color:'black', fontSize:16, marginTop:5}}>Canon PIXMA TS207 Inject</Text>
                  <View style={{flexDirection:'row', marginTop:5}}>
                    <Text style={{fontSize:12, marginLeft:1, marginRight:1}}>Durasi : </Text>
                    <Text style={{fontSize:12, marginLeft:1, marginRight:1}}>1 Bulan</Text>
                  </View>
                  <View style={{flexDirection:'row', marginTop:5}}>
                    <Text style={{fontSize:12, marginLeft:1, marginRight:1}}>Start Date : </Text>
                    <Text style={{fontSize:12, marginLeft:1, marginRight:1}}>Kamis, 1 Januari 2019</Text>
                  </View>
                  <View style={{flexDirection:'row', marginTop:5}}>
                    <Text style={{fontSize:12, marginLeft:1, marginRight:1}}>Status Pembayaran : </Text>
                    <Text style={{fontSize:12, marginLeft:1, marginRight:1}}>Belum Dibayar</Text>
                  </View>
                </View>
                <View style={{height:'30%', flexDirection:'row', alignItems:'center', justifyContent:'space-between', paddingTop:10}}>
                  <Text style={{color:'#65c5f2', fontWeight:'800'}}>Indineed</Text>
                  <TouchableOpacity style={{padding:10, borderRadius:20, borderColor:'#65c5f2', borderWidth:1, alignItems:'center', justifyContent:'center'}}>
                    <Text style={{color:'#65c5f2', fontWeight:'500'}}>   Lihat   </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>this.props.navigation.navigate('KosanDetail')} style={{marginTop:30, marginBottom:30, height:200, borderRadius:10, borderColor:'lightgray', borderWidth:1, flexDirection:'row', justifyContent:'space-between'}}>
              <View style={{height:'100%', width:'40%'}}>
                <View style={{position:'absolute', bottom:0, height:'115%', width:'100%', zIndex:1}}>
                  <Image style={{height:'100%', width:'100%', borderRadius:10, borderBottomRightRadius:0}} source={require("../../assets/img/indikos-search/kosan-1.png")}/>
                  <View style={{position:'absolute', bottom: 0, left: '10%', width:'80%', height:'18%', backgroundColor:'#65c5f2', alignItems:'center', justifyContent:'center', borderTopLeftRadius: 5, borderTopRightRadius: 5}}>
                    <Text style={{color:'white', fontWeight:'bold'}}>Tertunda</Text>
                  </View>
                </View>
              </View>
              <View style={{height:'100%', width:'60%', padding:10}}>
                <View style={{height:'70%', borderBottomColor:'lightgray', borderBottomWidth:1}}>
                  <Text style={{fontWeight:'bold', color:'black', fontSize:16, marginTop:5}}>Canon PIXMA TS207 Inject</Text>
                  <View style={{flexDirection:'row', marginTop:5}}>
                    <Text style={{fontSize:12, marginLeft:1, marginRight:1}}>Durasi : </Text>
                    <Text style={{fontSize:12, marginLeft:1, marginRight:1}}>1 Bulan</Text>
                  </View>
                  <View style={{flexDirection:'row', marginTop:5}}>
                    <Text style={{fontSize:12, marginLeft:1, marginRight:1}}>Start Date : </Text>
                    <Text style={{fontSize:12, marginLeft:1, marginRight:1}}>Kamis, 1 Januari 2019</Text>
                  </View>
                  <View style={{flexDirection:'row', marginTop:5}}>
                    <Text style={{fontSize:12, marginLeft:1, marginRight:1}}>Status Pembayaran : </Text>
                    <Text style={{fontSize:12, marginLeft:1, marginRight:1}}>Belum Dibayar</Text>
                  </View>
                </View>
                <View style={{height:'30%', flexDirection:'row', alignItems:'center', justifyContent:'space-between', paddingTop:10}}>
                  <Text style={{color:'#65c5f2', fontWeight:'800'}}>Indineed</Text>
                  <TouchableOpacity style={{padding:10, borderRadius:20, borderColor:'#65c5f2', borderWidth:1, alignItems:'center', justifyContent:'center'}}>
                    <Text style={{color:'#65c5f2', fontWeight:'500'}}>   Lihat   </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </ScrollView>
        <Maintabs
          navigate={this.props.navigation.navigate}
        />
      </View>
    );
  }
}

const styles= StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#eaf2ff',
  },
  oval: {
      height: Dimensions.get('window').width * 2,
      width: Dimensions.get('window').width * 3,
      marginLeft: -(Dimensions.get('window').width),
      borderBottomLeftRadius: Dimensions.get('window').width*1.5,
      borderBottomRightRadius: Dimensions.get('window').width*1.5,
      backgroundColor: 'rgba(26, 175, 244, 0.75)',
      position: 'absolute',
      top: -(Dimensions.get('window').width*0.4),
  },
});

const mapStateToProps = (state) => {
    return {
        data: state.loginReducer.data,
        isFetching: state.loginReducer.isFetching,
        error: state.loginReducer.error,
        message: state.loginReducer.message,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        finishCheck: () => dispatch(finishCheck()),
    };
};

export default connect(mapStateToProps,mapDispatchToProps)(TransactionHistoryList);
