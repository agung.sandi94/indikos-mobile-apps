import axios from 'axios'
import config from './config'

function getSkill(token){
    return axios.get(`${config.BASE_URL}/skill`,{
        headers: { Authorization: "Bearer " + token},
        timeout: 8000
    }).catch(error => {
		    return error.response;
	  });
}

export {
  getSkill
};
