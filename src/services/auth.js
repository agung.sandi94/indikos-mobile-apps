import axios from 'axios'
import config from './config'

function login(username,password){
    return axios.post(`${config.BASE_URL}/auth/login`, {
        email: username,
        password: password,
    },{
        timeout: 8000
    }).catch(error => {
		    return error.response;
	  });
}

function register(username,password,name,title_1,title_2){
    let body = {
      email: username,
      password: password,
      name: name,
      title_1: title_1,
      title_2: title_2
    }

    return axios.post(`${config.BASE_URL}/register`, body ,{
        timeout: 8000
    }).catch(error => {
		    return error.response;
	  });
}

export {
  login,
  register
};
